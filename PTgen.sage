#Compute indexes for Signed Comb method
def PTgen(w = 8, t = 256):
	d = t/w
	TC = [[0 for j in range (0, t)] for i in range(0, w)]
	for i in range(0, w):
		val = 2^i
		for j in range(0, t):
			bits = binary(j)
			for k in range (0, len(bits)):
				if bits[k] == 1:
					dis = w*(k)
					TC[i][j] += val << dis

	return TC



# print PT in hex
def printPT(PT, w = 8, t = 256):
	for i in range(0, w):
		for j in range(0, t):
			print hex(PT[i][j]),
		print

