from math import *
from random import *

NTEST = 1000
NTEST2 = 50

"""
Op counters
"""
fpa  = 0
fpm  = 0
fpi  = 0
fpmb = 0

SAGE_COUNT_OP = 1

fpe = 0



"""
Load Fp arithmetic
"""
attach("fp12.sage")


"""
Load the parameters
"""
attach("params.sage")


"""
Load G1 arithmetic
"""
attach("ecn.sage")


print "################################################################"
print "# Test G1 arithmetic"
print "################################################################"


print "\nG1_rand"
for i in [1..NTEST2]:
	P = G1_rand()
	Q = G1_toJ(P)
	R = G1_mulDA(q, Q)
	if R != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nG1_rand Det"
for i in [1..NTEST2]:
	P = G1_randDet()
	Q = G1_toJ(P)
	R = G1_mulDA(q, Q)
	if R != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest wNAF"
for i in [1..NTEST]:
	k = rand() % (2^64)
	mp, mn, tlen = NAFw64(k, WNAF)

	acum = 0
	for i in range(0, tlen):
		acum = acum + mp[i] * (2^i)

	acum2 = 0
	for i in range(0, tlen):
		acum2 = acum2 + mn[i] * (2^i)

	acum -= acum2
	diff = k - acum

	if diff != 0:
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0

#n = 13
#R0 = G1_norm(G1_addJJJ(G1_dblJ(G1_dblJ(G1_dblJ(G1_dblJ(G1_toJ(P),p),p),p),p), G1_negJ(G1_addJJA(G1_dblJ(G1_toJ(P),p),P,p),p),p),p)


print "\nTest DA+wNAF-64bits"
for i in [1..NTEST2]:
	n = rand() % (2^64)
	R1 =  G1_norm(G1_mulDA(k, G1_toJ(P)))
	R2 =  G1_norm(G1_mulDAwNAF(k, G1_toJ(P)))

	diff = G1_addJJJ(R1, G1_negJ(R2))
	if diff != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest E->E"
for i in [1..NTEST2]:
	Q  = G1_rand()
	R1 = G1_norm(G1_mulDA(llambda, G1_toJ(Q)))
	R2 = G1_toJ([Fp_mulC(bbeta, Q[0]), Q[1]])

	diff = G1_addJJJ(R1, G1_negJ(R2))
	if diff != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest GLV"
for i in [1..NTEST2]:
	n  = rand()
	Q  = G1_rand()

	u  = G1Decomp(n, W, SB)
	Q0 = G1_mulDA(u[0].abs(), G1_toJ(Q))
	Q1 = G1_mulDA(u[1].abs(), [Fp_mulC(bbeta, Q[0]), Q[1], 1])
	if u[0] < 0:
		Q0 = G1_negJ(Q0)
	if u[1] < 0:
		Q1 = G1_negJ(Q1)
	R1 = G1_norm(G1_addJJJ(Q0, Q1))
	R2 = G1_norm(G1_mulDA(n, G1_toJ(Q)))

	diff = G1_addJJJ(R1, G1_negJ(R2))
	if diff != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest GLV+wNAF"
for i in [1..1]:
	n  = rand()
	Q  = G1_rand()
	R1 = G1_norm(G1_mulGLVwNAF(n, G1_toJ(Q)))
	R2 = G1_norm(G1_mulDA(n, G1_toJ(Q)))

	diff = G1_addJJJ(R1, G1_negJ(R2))
	if diff != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest Simultaneous normalisation"
n = 256
PJ = [G1_mulDA(rand(),G1_toJ(G1_rand())) for i in range(0, n)]
PN = MultiNormalizeFp(PJ, n)
for i in range(0, n):
	diff = G1_addJJJ(G1_norm(PJ[i]), G1_negJ(G1_toJ(PN[i])))
	if diff != G1_zeroJ():
		fpe +=1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0


print "\nTest Signed Comb mul"
for i in [1..NTEST2]:
	P = G1_rand()
	n = rand()
	PP = G1_precomputeSC(P)
	R1 = G1_norm(G1_mulDA(n, G1_toJ(P)))
	R2 = G1_norm(G1_mulSC(n, PP, PT, t256, Half_q))
	diff = G1_addJJJ(R1, G1_negJ(R2))
	if diff != G1_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0
