from math import *
from random import *

NTEST = 1000
NTEST2 = 50

"""
Op counters
"""
fpa  = 0
fpm  = 0
fpi  = 0
fpmb = 0

SAGE_COUNT_OP = 1

fpe = 0


"""
Load arithmetic
"""
attach("fp12.sage")



"""
Load the parameters
"""
attach("params.sage")



print "################################################################"
print "# Test Fp arithmetic"
print "################################################################"


print "\nFp_add/sub"
for i in [1..NTEST]:
	a1 = rand()
	b1 = rand()
	c1 = Fp_addC(a1, b1)
	c1 = Fp_subC(c1, b1)
	diff = Fp_subC(c1, a1)
	if diff != Fp_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0


print "\nFp_neg"
for i in [1..NTEST]:
	a1 = rand()
	b1 = Fp_negC(a1)
	c1 = Fp_addC(a1, b1)
	if diff != Fp_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp_mul/square"
for i in [1..NTEST*10]:
	a1 = rand()
	b1 = Fp_square(a1)
	c1 = Fp_mulC(a1, a1)
	diff = Fp_subC(b1, c1)
	if diff != Fp_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp_inv/mul"
for i in [1..NTEST*10]:
	a1 = rand()
	b1 = Fp_inv(a1)
	diff = Fp_mulC(a1, b1)
	if diff != Fp_one():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp_exp order"
for i in [1..NTEST2]:
	A = rand()
	diff = Fp_subC(Fp_mulC(Fp_exp(A, 3), Fp_inv(A)), Fp_square(A))
	if diff != Fp_zero():
		fpe = fpe + 1
		#print error

	diff = Fp_subC(A, Fp_exp(A, p))
	if diff != Fp_zero():
		fpe = fpe + 1
		#print error


if fpe != 0:
	print fpe, "errors!"
	#raise SystemExit(0)
else:
	print "OK"
fpe = 0



print "\nFp_sqrt (p=3mod4)"
for i in [1..NTEST2]:
	A  = rand()

	t0 = Fp_SQRTp3m4(A, 1)

	if t0 != -1:
		diff = Fp_subC(A, Fp_square(t0))
		if diff != Fp_zero():
			fpe = fpe + 1
			#print error


if fpe != 0:
	print fpe, "errors!"
	#raise SystemExit(0)
else:
	print "OK"
fpe = 0



print "################################################################"
print "# Test Fp2 arithmetic"
print "################################################################"

print "\nFp2_add/sub"
for i in [1..NTEST]:
	A = rand2()
	B = rand2()
	C = Fp2_addC(A, B)
	C = Fp2_subC(C, B)
	diff = Fp2_subC(C, A)
	if diff != Fp2_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0


print "\nFp2_neg"
for i in [1..NTEST]:
	A = rand2()
	B = Fp2_neg(A)
	C = Fp2_addC(A, B)
	if diff != Fp2_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0


print "\nFp2_mul/square"
for i in [1..NTEST]:
	A = rand2()
	A1 = Fp2_square(A)
	A2 = Fp2_mul(A, A)
	diff = (A1[0] - A2[0]) + (A1[1] -A2[1])
	if diff != 0:
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp2_inv/mul"
for i in [1..NTEST]:
	A = rand2()
	B = Fp2_inv(A)
	diff = Fp2_mul(A,B)
	if diff != Fp2_one():
		fpe = fpe + 1
		#print error

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp2_exp order"
for i in [1..NTEST2]:
	A = rand2()
	diff = Fp2_subC(Fp2_mul(Fp2_exp(A, 3), Fp2_inv(A)), Fp2_square(A))
	if diff != Fp2_zero():
		fpe = fpe + 1
		#print error

	diff = Fp2_subC(A, Fp2_conj(Fp2_exp(A, p)))
	if diff != Fp2_zero():
		fpe = fpe + 1
		raise SystemExit(0)
		#print error

AA = rand2()
BB = AA[:2]
# work around to exponentiate to p^k
for i in range(1, k+1):
	BB = Fp2_exp(BB, p)
	#print i

diff = Fp2_subC(AA, BB)
if diff != Fp2_zero():
	fpe = fpe + 1
	#print error



if fpe != 0:
	print fpe, "errors!"
	#raise SystemExit(0)
else:
	print "OK"
fpe = 0



print "################################################################"
print "# Test Fp6 arithmetic"
print "################################################################"

print "\nFp6_add/sub"
for i in [1..NTEST]:
	AA = rand6()
	BB = rand6()

	CC = Fp6_zero()

	CC = Fp6_add(AA, BB)
	CC = Fp6_sub(CC, BB)
	diff = Fp6_sub(CC, AA)
	if diff != Fp6_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0

print "\nFp6_mul/square"
for i in [1..NTEST]:
	AA = rand6()
	CC = Fp6_mul(AA, AA)

	diff = Fp6_sub(CC, Fp6_square(AA))
	if diff != Fp6_zero():
			fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp6_inv/mul"
for i in [1..NTEST]:
	AA = rand6()
	CC = Fp6_inv(AA)
	diff = Fp6_mul(CC, AA)
	if diff != Fp6_one():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp6_exp order"
for i in [1..NTEST2]:
	AA = rand6()
	diff = Fp6_sub(Fp6_mul(Fp6_exp(AA, 3), Fp6_inv(AA)), Fp6_square(AA))
	if diff != Fp6_zero():
		fpe = fpe + 1
		#print error

AA = rand6()
BB = AA[:6]
for i in range(1, k+1):
	BB = Fp6_exp(BB, p)

diff = Fp6_sub(AA, BB)
if diff != Fp6_zero():
	fpe = fpe + 1
	#print error



if fpe != 0:
	print fpe, "errors!"
	#raise SystemExit(0)
else:
	print "OK"
fpe = 0



print "################################################################"
print "# Test Fp12 arithmetic"
print "################################################################"

print "\nFp12_add/sub"
for i in [1..NTEST]:
	AA = rand12()
	BB = rand12()

	CC = Fp12_zero()

	CC = Fp12_add(AA, BB)
	CC = Fp12_sub(CC, BB)
	diff = Fp12_sub(CC, AA)
	if diff != Fp12_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp12_mul/square"
for i in [1..NTEST]:
	AA = rand12()

	CC = Fp12_zero()
	CC = Fp12_mul(AA, AA)
	if CC == Fp12_zero():
		break
	diff = Fp12_sub(CC, Fp12_square(AA))
	if diff != Fp12_zero():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp12_inv/mul"
for i in [1..NTEST]:
	AA = rand12()

	CC = Fp12_zero()
	CC = Fp12_inv(AA)
	diff = Fp12_mul(CC, AA)
	if diff != Fp12_one():
		fpe = fpe + 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFp12_exp order"
for i in [1..NTEST2]:
	AA = rand12()
	diff = Fp12_sub(Fp12_mul(Fp12_exp(AA, 3), Fp12_inv(AA)), Fp12_square(AA))
	if diff != Fp12_zero():
		fpe = fpe + 1
		#print error

AA = rand12()
BB = AA[:2]
for i in range(1, k+1):
	BB = Fp12_exp(BB, p)
	if i == 6:
		if AA != Fp12_conj(BB):
			fpe = fpe + 1
			#print error


diff = Fp12_sub(AA, BB)
if diff != Fp12_zero():
	fpe = fpe + 1
	#print error


if fpe != 0:
	print fpe, "errors!"
	#raise SystemExit(0)
else:
	print "OK"
fpe = 0
