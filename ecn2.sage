attach("ecn.sage")

"""
ECn2 arithmetic
"""







#non-deterministic
def hash_and_mapE2(ID):
	global B
	x0 = Fp2_zero()
	x0[0] = 1
	x0[1] = H1(ID)
	
	while 1:
		x3 = Fp2_square(x0)
		x3 = Fp2_mul(x3, x0)
		x3 = Fp2_addC(x3, B_invChi) #x^3+2
		y0 = Fp2_SQRT(x3)
		if y0 == [-1, 0]:
			x0 = Fp2_addC(x0, Fp2_one())
			#print "."
		else:
			break

	return [x0, y0]



#non-deterministic
def G2_rand():
	ID = str(rand())
	Q = hash_and_mapE2(ID)
	Q = HashtoG2(Q)

	return Q



def Xp2(a):
	b    = Fp2_copy(a)
	b[1] = Fp_negC(b[1])
	b    = Fp2_mul(b, a)

	return Legendre(b[0], p)




#deterministic
def hash_and_mapE2Det(ID):
	global B
	global SQRTm3
	global c0

	t = Fp2_zero()

	t[0] = H1(ID)
	t[1] = Fp_copy(t[0])

	w = Fp2_square(t)
	w = Fp2_addC(w, B_invChi)
	w[0] += 1
	w = Fp2_inv(w)
	w = Fp2_mul(w, t)
	w = Fp2_mul(w, [SQRTm3, Fp_zero()])

	x    = [Fp2_zero(), Fp2_zero(), Fp2_zero()]
	x[0] = Fp2_mul(w, t)
	x[0] = Fp2_subC(x[0], [c0, Fp_zero()])
	x[0] = Fp2_neg(x[0])

	x[1] = Fp2_addC(x[0], Fp2_one())
	x[1] = Fp2_neg(x[1])

	x[2] = Fp2_square(w)
	x[2] = Fp2_inv(x[2])
	x[2] = Fp2_addC(x[2], Fp2_one())

	a    = [Fp2_zero() for i in range(0,3)]
	for i in range(0, 3):
		a[i] = Fp2_square(x[i])
		a[i] = Fp2_mul(a[i], x[i])
		a[i] = Fp2_addC(a[i], B_invChi)

	alpha    = [0 for i in range(0, 2)]
	alpha[0] = Xp2(a[0])
	alpha[1] = Xp2(a[1])

	ii  = alpha[0] - 1
	ii *= alpha[1]
	ii %= 3

	Q0  = x[ii]
	c   = Xp2(t)
	d   = Fp2_SQRT(a[ii])

	if c<0:
		Q1 = Fp2_neg(d)
	else:
		Q1 = d


	return [Q0, Q1]



#Deterministic
def G2_randDet():
	ID = str(rand())
	Q = hash_and_mapE2Det(ID)
	Q = HashtoG2(Q)

	return Q



# J <- J + J
def G2_addJJJ(P, Q):
	R = G2_zeroJ()

	if P == G2_zeroJ():
		return G2_copyJ(Q)

	if Q == G2_zeroJ():
		return G2_copyJ(P)

	Z1Z1 = Fp2_square(P[2])
	Z2Z2 = Fp2_square(Q[2])
	U1 = Fp2_mul(P[0], Z2Z2)
	U2 = Fp2_mul(Q[0], Z1Z1)
	t0 = Fp2_mul(Q[2], Z2Z2)
	S1 = Fp2_mul(P[1], t0)
	t1 = Fp2_mul(P[2], Z1Z1)
	S2 = Fp2_mul(Q[1], t1)
	H  = Fp2_subC(U2, U1)
	t3 = Fp2_subC(S2, S1)

	if H == Fp2_zero():
		if t3 == Fp2_zero():
			R = G2_dblJ(P)
		#else:
			# R = G2_zeroJ()

		return R

	t2 = Fp2_addC(H, H)
	I  = Fp2_square(t2)
	J  = Fp2_mul(H, I)
	r  = Fp2_addC(t3, t3)
	V  = Fp2_mul(U1, I)
	t4 = Fp2_square(r)
	t5 = Fp2_addC(V, V)
	t6 = Fp2_subC(t4, J)
	R[0] = Fp2_subC(t6, t5)
	t7 = Fp2_subC(V, R[0])
	t8 = Fp2_mul(S1, J)
	t9 = Fp2_addC(t8, t8)
	t10  = Fp2_mul(r, t7)
	R[1] = Fp2_subC(t10, t9)
	t11  = Fp2_addC(P[2], Q[2])
	t12  = Fp2_square(t11)
	t13  = Fp2_subC(t12, Z1Z1)
	t14  = Fp2_subC(t13, Z2Z2)
	R[2] = Fp2_mul(t14, H)

	return R


def G2_dblJ(P):
	R = G2_zeroJ()

	if P == G2_zeroJ():
		return R

	A  = Fp2_square(P[0])
	B  = Fp2_square(P[1])
	C  = Fp2_square(B)
	t0 = Fp2_addC(P[0], B)
	t1 = Fp2_square(t0)
	t2 = Fp2_subC(t1, A)
	t3 = Fp2_subC(t2, C)
	D  = Fp2_addC(t3, t3)
	E  = Fp2_addC(A, A)
	E  = Fp2_addC(E, A)
	F  = Fp2_square(E)
	t4 = Fp2_addC(D, D)
	R[0] = Fp2_subC(F, t4)
	t5 = Fp2_subC(D, R[0])
	t6 = C
	t6 = Fp2_addC(t6, t6)#t6+= t6
	t6 = Fp2_addC(t6, t6)#t6+= t6
	t6 = Fp2_addC(t6, t6)
	#t6+= t6
	#t6 = t6 % p
	t7 = Fp2_mul(E, t5)
	t8 = Fp2_mul(P[1], P[2])
	R[1] = Fp2_subC(t7, t6)
	R[2] = Fp2_addC(t8, t8)

	return R



# J <- J + A
def G2_addJJA(P, Q):
	R = G2_zeroJ()
	if P == G2_zeroJ():
		if Q == G2_zeroA():
			return R #G1_zeroJ()
		else:
			return G2_toJ(G2_copyA(Q))
	if Q == G2_zeroA():
		return G2_copyJ(P)

	t1 = Fp2_square(P[2])
	t2 = Fp2_mul(t1, P[2])
	t1 = Fp2_mul(t1, Q[0])
	t2 = Fp2_mul(t2, Q[1])
	t1 = Fp2_subC(t1, P[0])
	t2 = Fp2_subC(t2, P[1])

	if t1 == Fp2_zero():
		if t2 != Fp2_zero():
			return R#G2_zeroJ()
		else:
			R = G2_toJ(G2_copyA(Q))
			return G2_dblJ(R)

	R[2] = Fp2_mul(P[2], t1)
	t3   = Fp2_square(t1)
	t4   = Fp2_mul(t3, t1)
	t3   = Fp2_mul(t3, P[0])
	t1   = Fp2_addC(t3, t3)
	R[0] = Fp2_square(t2)
	R[0] = Fp2_subC(R[0], t1)
	R[0] = Fp2_subC(R[0], t4)
	t3   = Fp2_subC(t3, R[0])
	t3   = Fp2_mul(t3, t2)
	t4   = Fp2_mul(t4, P[1])
	R[1] = Fp2_subC(t3, t4)

	return R



# Point-at-infinity in Jacobians
def G2_zeroJ():

	return [Fp2_one(), Fp2_one(), Fp2_zero()]



# Point-at-infinity in Affine
def G2_zeroA():

	return [Fp2_zero(), Fp2_one()]



# -P in Jacobians
def G2_negJ(P):

	return [P[0], Fp2_neg(P[1]), P[2]]



# -P in Affine
def G2_negA(P):

	return [P[0], Fp2_neg(P[1])]



# Safe point copy in Jacobians
def G2_copyJ(P):
	R = G2_zeroJ()
	R[0] = P[0]
	R[1] = P[1]
	R[2] = P[2]

	return R



# Safe point copy in Affine
def G2_copyA(P):
	R = G2_zeroA()
	R[0] = P[0]
	R[1] = P[1]

	return R



# Normalisation of point
def G2_norm(P):
	R = G2_zeroJ()
	if P[2] == Fp2_zero():
		return G2_zeroJ()

	if P[2] == Fp2_one():
		return G2_copyJ(P)

	Z = P[2]
	Z = Fp2_inv(Z)
	Z2 = Fp2_square(Z)
	Z3 = Fp2_mul(Z2, Z)
	R[0] = Fp2_mul(P[0], Z2)
	R[1] = Fp2_mul(P[1], Z3)
	R[2] = Fp2_one()

	return R



# J <- A
def G2_toJ(P):
	R = G2_zeroJ()
	R[0] = P[0]
	R[1] = P[1]
	R[2] = Fp2_one()

	return R



# Double-and-Add scalar-point multiplication
# J <- J
def G2_mulDA(k, P):
	#R = G2_zeroJ()
	if k == 0:
		return G2_zeroJ()
	if k == 1:
		return P
	if k == 2:
		R = G2_dblJ(P)
		return R

	lbin = k.nbits()
	bin_k= k.bits()
	R = G2_copyJ(P)
	for i in range(lbin-2,-1,-1):
		R = G2_dblJ(R)
		if bin_k[i] == 1:
			R = G2_addJJJ(R, P) #JJA

	return R



def p_power_FrobeniusA(P):
	Q = G2_copyA(P)
	for i in range(0,2):
		Q[i] = Fp2_conj(Q[i])
	for i in range(0,2):
		Q[i] = Fp2_mul(Q[i], V[i])

	return Q

def p_power_FrobeniusJ(P):
	Q = G2_copyJ(P)
	for i in range(0,3):
		Q[i] = Fp2_conj(Q[i])
	for i in range(0,2):
		Q[i] = Fp2_mul(Q[i], V[i])

	return Q

def p_power_FrobeniusJi(P, n):
	Q = G2_copyJ(P)
	for i in range(0,n):
		Q = p_power_FrobeniusJ(Q)

	return Q

def HashtoG2(P):
	Px = G2_toJ(P)
	Px = G2_mulDA(z.abs(), Px)
	if z < 0:
		Px = G2_negJ(Px)
	P3x= G2_dblJ(Px)
	P3x= G2_addJJJ(P3x, Px)
	P3x= p_power_FrobeniusJ(P3x)
	Q  = G2_addJJJ(Px, P3x)
	Px = p_power_FrobeniusJi(Px, 2)
	Q  = G2_addJJJ(Q, Px)
	Px = G2_toJ(G2_copyA(P))
	Px = p_power_FrobeniusJi(Px, 3)
	Q = G2_addJJJ(Q, Px)

	Q = G2_norm(Q)
	return Q




def G2Decomp(k, W, BB):
	v0 = [0 for i in range(0,4)]
	u  = [0 for i in range(0,4)]

	for i in range(0,4):
		v0[i] = notsomadSR(W[i], k)
		u[i]  = 0
	u[0]  = k
	
	for i in range(0,4):
		for j in range(0,4):
			u[i] = u[i] - v0[j] * BB[j][i]

	return u




# Precompute for wNAF, w=3
def G2_precomputeDA3NAF(P):
	Pi = [G2_zeroJ() for i in range (2)]
	Pi[0] = G2_copyJ(P)
	Pi[1] = G2_dblJ(Pi[0])
	Pi[1] = G2_addJJA(Pi[1], Pi[0])
	Pi[1] = G2_norm(Pi[1])

	return Pi



# GS method, GS decomposition
# J <- J
def G2_mulGSwNAF(k, P, WB, BB):
	#global WNAF
	#R = G1_zeroJ()
	if k == 0:
		return G2_zeroJ()
	if k == 1:
		return P
	if k == 2:
		R = G2_dblJ(P)
		return R

	u = G2Decomp(n, WB, BB)
	mp = [[0 for j in range(0, 130)] for i in range(0, 4)]
	mn = [[0 for j in range(0, 130)] for i in range(0, 4)]
	tlen = [0 for i in range(0,  4)]

	for i in range(0, 4):
		mp[i], mn[i], tlen[i] = NAFw(u[i].abs(), 3)

	tlent = tlen[0]
	for i in range(1,4):
		if tlen[i] > tlent:
			tlent = tlen[i]
	
	Pi = [[G2_zeroJ() for i in range(0, 2)] for j in range (0, 4)]

#	if (P[2] != Fp2_one()):
#		P = G2_norm(P, p)

	P0    = [G2_zeroJ() for i in range(0, 4)]
	P0[0] = G2_copyJ(P)
	for i in range(1,4):
		P0[i] = p_power_FrobeniusJ(P0[i-1])
	for i in range(0, 4):
		if u[i] < 0:
			P0[i] = G2_negJ(P0[i])

	for i in range(0, 4):
		Pi[i] = G2_precomputeDA3NAF(P0[i])


	R = G2_zeroJ()
	for i in range(tlent-1,-1,-1):
		R = G2_dblJ(R)
		for j in range(0,4):
			if mp[j][i] != 0:
				R = G2_addJJA(R, Pi[j][mp[j][i]>>1])
			if mn[j][i] != 0:
				R = G2_addJJA(R, G2_negJ(Pi[j][mn[j][i]>>1]))

	return R



#simultaneous normalisation using Montgomery's trick
# A <- J
def MultiNormalizeFp2(PJ, n):
	PN = [G2_zeroA() for i in range(0, n)]
	if n == 0:
		return PN
	if n == 1:
		P0 = G2_norm(PJ[0])
		PN[0][0] = P0[0]
		PN[0][1] = P0[1]
		return P

	t = [1 for i in range(0, 2*n)]
	d = PJ[0][2]

	#(n-1)*(m)
	for i in range(1, n):
		d = Fp2_mul(d, PJ[i][2])
	d = Fp2_inv(d)
	t[1]     = PJ[0][2]
	t[n + 1] = PJ[n-1][2]

	#(n-2)*(2*m)
	for i in range(2, n):
		t[i]     = Fp2_mul(t[i - 1], PJ[i - 1][2])
		t[n + i] = Fp2_mul(t[n + i - 1], PJ[n - i][2])

	#(n-2)*(2*m)
	for i in range(1, n-1):
		t[i] = Fp2_mul(t[i], t[2 * n - i - 1])
	t[0] = t[2 * n - 1]

	# (n) * (4*m + s)
	for i in range(0, n):
		t0 = Fp2_mul(d, t[i])
		d2 = Fp2_square(t0)
		d3 = Fp2_mul(d2, t0)
		PN[i][0] = Fp2_mul(PJ[i][0], d2)
		PN[i][1] = Fp2_mul(PJ[i][1], d3)
		#PN[i][2] = 1


	return PN


def G2_precomputeSC(P):
	t = 256
	d = 32
	PQ = [G2_zeroJ() for i in range (0, t)]
	PP = [G2_zeroJ() for i in range (0, t//2)]

	PQ[0] = [Fp2_one(), Fp2_one(), Fp2_one()] #for normalisation
	PQ[1] = G2_copyJ(G2_toJ(P))

	td = 2^d
	PQ[2] = G2_mulDA(td, PQ[1])
	pp1 = 2
	for i in range(3, t):
		if i==(1<<pp1):
			PQ[i] = G2_mulDA(td, PQ[(1<<(pp1-1))])
			pp1 += 1
		else:
			PQ[i] = PQ[(1<<(pp1-1))]
			pp2 = i
			log2i = i.bit_length()#int(log(i,2))#i.bit_length() ?
			for j in range (0, log2i-1):
				if pp2&1 == 1:
					PQ[i] = G2_addJJJ(PQ[i], PQ[(1<<j)])
				pp2 >>= 1

	for i in range(0, t//2):
		PQ[i] = G2_addJJJ(PQ[i], G2_negJ(PQ[t-1-i]))

	PP = MultiNormalizeFp2(PQ, t//2)
	PP[0] = G2_negJ(G2_norm(PQ[255]))

	return PP



def G2_mulSC(k, PP, PT, t256, Half_q):
	R = G2_zeroJ()
	Q = G2_zeroA()
	acum = RecodeForPrecompSC(k, PT, t256, Half_q)

	for i in range(31,-1,-1):
		R = G2_dblJ(R)
		if ((acum[i]&0x80) == 0x80):
			Q[0] = PP[acum[i]^^0xFF][0]
			Q[1] = PP[acum[i]^^0xFF][1]
			Q = G2_negA(Q)
			R = G2_addJJA(R, Q)
		else:
			Q[0] = PP[acum[i]][0] #|0x00
			Q[1] = PP[acum[i]][1]
			Q = G2_copyA(Q)
			R = G2_addJJA(R, Q)

	return R

