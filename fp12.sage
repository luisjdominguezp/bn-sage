attach("fp6.sage")

"""
Fp12 arithmetic
Fp12: Fp2[w]/w^6 - gamma
gamma = v \in Fp6
"""

def gamma_mul(A):
	t0 = chi_mul(A[2])

	return [t0, A[0], A[1]]



def Fp12_add(a, b):
	c = Fp12_zero()
	c[0] = Fp6_add(a[0], b[0])
	c[1] = Fp6_add(a[1], b[1])

	return c



def Fp12_sub(a, b):
	c = Fp12_zero()
	c[0] = Fp6_sub(a[0], b[0])
	c[1] = Fp6_sub(a[1], b[1])

	return c



def Fp12_mul(a, b):
	v0 = Fp6_mul(a[0], b[0])
	v1 = Fp6_mul(a[1], b[1])
	t0 = gamma_mul(v1)
	c0 = Fp6_add(v0, t0)
	t0 = Fp6_add(a[0], a[1])
	t1 = Fp6_add(b[0], b[1])
	c1 = Fp6_mul(t0, t1)
	c1 = Fp6_sub(c1, v0)
	c1 = Fp6_sub(c1, v1)

	return [c0, c1]



def Fp12_square(a):
	t0 = Fp6_sub(a[0], a[1])
	t1 = gamma_mul(a[1])
	t1 = Fp6_sub(a[0], t1)
	t2 = Fp6_mul(a[0], a[1])
	t0 = Fp6_mul(t0, t1)
	t0 = Fp6_add(t0, t2)
	t1 = Fp6_add(t2, t2)
	t2 = gamma_mul(t2)
	t0 = Fp6_add(t0, t2)

	return [t0, t1]



def Fp12_inv(a):
	t0 = Fp6_square(a[0])
	t1 = Fp6_square(a[1])
	t1 = gamma_mul(t1)
	t0 = Fp6_sub(t0, t1)
	t1 = Fp6_inv(t0)
	c0 = Fp6_mul(a[0], t1)
	c1 = Fp6_mul(a[1], t1)
	t0 = [[0,0],[0,0],[0,0]] #for conj()
	c1 = Fp6_sub(t0, c1)

	return [c0, c1]



def Fp12_exp(f, e):
	if e == 0:
		return 1

	if e ==  1:
		return f

	if e == 2:
		g = Fp12_square(f)

	lbin = e.nbits()
	bin_e= e.bits()
	g = f[:2]
	for i in range(lbin-2,-1,-1):
		g = Fp12_square(g)
		if bin_e[i] == 1:
			g = Fp12_mul(g, f)

	return g



def Fp12_conj(A):
	C = A[:2]
	C[1] = Fp6_sub(Fp6_zero(), A[1])
	return C



def Fp12_zero():
	A = [Fp6_zero(), Fp6_zero()]

	return A



def Fp12_one():
	A = [Fp6_one(), Fp6_zero()]

	return A



def Fp12_copy(A):
	B = Fp12_zero()
	B[0] = Fp6_copy(A[0])
	B[1] = Fp6_copy(A[1])

	return B



def rand12():
	A = [rand6(), rand6()]
	return A



def Fp12_powq(g, X):
	f = g[:2]
	W = Fp2_mul(X, X)
	f[0] = Fp6_powq(f[0], W)
	f[1] = Fp6_powq(f[1], W)
	f[1] = Fp6_mul(f[1], [X, Fp2_zero(), Fp2_zero()])

	return f

