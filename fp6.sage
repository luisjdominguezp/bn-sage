attach("fp2.sage")

"""
Fp4 arithmetic
Fp4: Fp2[v]/v^2 - chi
chi = u+1 \in Fp2
"""

def Fp4_square(A):
	t0 = Fp2_square(A[0])
	t1 = Fp2_square(A[1])
	c0 = chi_mul(t1)
	c0 = Fp2_addC(c0, t0)
	c1 = Fp2_addC(A[0], A[1])
	c1 = Fp2_square(c1)
	c1 = Fp2_subC(c1, t0)
	c1 = Fp2_subC(c1, t1)

	return [c0, c1]




"""
Fp6 arithmetic
Fp6: Fp2[v]/v^3 - chi
chi = u+1 \in Fp2
"""

def chi_mul(A):
	t0 = Fp_mulC(A[0], chi[0])
	t1 = beta_mul(A[1])
	c0 = Fp_addC(t0, t1)
	t0 = Fp_mulC(A[1], chi[0])
	c1 = Fp_addC(A[0], t0)

	return [c0, c1]



def Fp6_add(a, b):
	c = Fp6_zero()
	c[0] = Fp2_addC(a[0], b[0])
	c[1] = Fp2_addC(a[1], b[1])
	c[2] = Fp2_addC(a[2], b[2])

	return c



def Fp6_sub(a, b):
	c = Fp6_zero()
	c[0] = Fp2_subC(a[0], b[0])
	c[1] = Fp2_subC(a[1], b[1])
	c[2] = Fp2_subC(a[2], b[2])

	return c



def Fp6_mul(a, b):
	v0 = Fp2_mul(a[0], b[0])
	v1 = Fp2_mul(a[1], b[1])
	v2 = Fp2_mul(a[2], b[2])
	t0 = Fp2_addC(a[1], a[2])
	t1 = Fp2_addC(b[1], b[2])
	c0 = Fp2_mul(t0, t1)
	c0 = Fp2_subC(c0, v1)
	c0 = Fp2_subC(c0, v2)
	t0 = chi_mul(c0)
	c0 = Fp2_addC(v0, t0)

	t0 = Fp2_addC(a[0], a[1])
	t1 = Fp2_addC(b[0], b[1])
	c1 = Fp2_mul(t0, t1)
	c1 = Fp2_subC(c1, v0)
	c1 = Fp2_subC(c1, v1)
	t0 = chi_mul(v2)
	c1 = Fp2_addC(c1, t0)

	t0 = Fp2_addC(a[0], a[2])
	t1 = Fp2_addC(b[0], b[2])
	c2 = Fp2_mul(t0, t1)
	c2 = Fp2_subC(c2, v0)
	c2 = Fp2_subC(c2, v2)
	c2 = Fp2_addC(c2, v1)

	return [c0, c1, c2]



def Fp6_mul_Fp2(a, b):
	c0 = Fp2_mul(a[0], b)
	c1 = Fp2_mul(a[1], b)
	c2 = Fp2_mul(a[2], b)

	return [c0, c1, c2]



def Fp6_square(a):
	v4 = Fp2_mul(a[0], a[1])
	v4 = Fp2_addC(v4, v4)
	v5 = Fp2_square(a[2])
	v1 = chi_mul(v5)
	v1 = Fp2_addC(v1, v4)
	v2 = Fp2_subC(v4, v5)
	v3 = Fp2_square(a[0])
	v4 = Fp2_subC(a[0], a[1])
	v4 = Fp2_addC(v4, a[2])
	v5 = Fp2_mul(a[1], a[2])
	v5 = Fp2_addC(v5, v5)
	v4 = Fp2_square(v4)
	v0 = chi_mul(v5)
	v0 = Fp2_addC(v0, v3)
	v2 = Fp2_addC(v2, v4)
	v2 = Fp2_addC(v2, v5)
	v2 = Fp2_subC(v2, v3)

	return [v0, v1, v2]



def Fp6_inv(a):
	t0 = Fp2_square(a[0])
	t1 = Fp2_square(a[1])
	t2 = Fp2_square(a[2])
	t3 = Fp2_mul(a[0], a[1])
	t4 = Fp2_mul(a[0], a[2])
	t5 = Fp2_mul(a[1], a[2])
	t6 = chi_mul(t5)
	c0 = Fp2_subC(t0, t6)
	t6 = chi_mul(t2)
	c1 = Fp2_subC(t6, t3)
	c2 = Fp2_subC(t1, t4)
	t7 = Fp2_mul(a[0], c0)
	t8 = Fp2_mul(a[2], c1)
	t8 = chi_mul(t8)
	t7 = Fp2_addC(t7, t8)
	t8 = Fp2_mul(a[1], c2)
	t8 = chi_mul(t8)
	t7 = Fp2_addC(t7, t8)
	t7 = Fp2_inv(t7)

	c0 = Fp2_mul(c0, t7)
	c1 = Fp2_mul(c1, t7)
	c2 = Fp2_mul(c2, t7)
	
	return [c0, c1, c2]



def Fp6_exp(f, e):
	if e == 0:
		return 1

	if e ==  1:
		return f

	if e == 2:
		g = Fp6_square(f)

	lbin = e.nbits()
	bin_e= e.bits()
	g = f[:3]
	for i in range(lbin-2,-1,-1):
		g = Fp6_square(g)
		if bin_e[i] == 1:
			g = Fp6_mul(g, f)

	return g



def Fp6_zero():
	A = [Fp2_zero(), Fp2_zero(), Fp2_zero()]

	return A



def Fp6_one():
	A = [Fp2_one(), Fp2_zero(), Fp2_zero()]

	return A



def Fp6_copy(A):
	B = Fp6_zero()
	B[0] = Fp2_copy(A[0])
	B[1] = Fp2_copy(A[1])
	B[2] = Fp2_copy(A[2])

	return B



def rand6():
	A = [rand2(), rand2(), rand2()]
	return A



def Fp6_powq(g, W):
	f = g[:3]
	f[0] = Fp2_conj(f[0])
	f[1] = Fp2_conj(f[1])
	f[2] = Fp2_conj(f[2])
	f[1] = Fp2_mul(f[1], W)
	W2   = Fp2_mul(W, W)
	f[2] = Fp2_mul(f[2], W2)

	return f

