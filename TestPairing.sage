from math import *
from random import *

NTEST = 1000
NTEST2 = 50

"""
Op counters
"""
fpa  = 0
fpm  = 0
fpi  = 0
fpmb = 0

SAGE_COUNT_OP = 1

fpe = 0



"""
Load Fp arithmetic
"""
attach("fp12.sage")


"""
Load the parameters
"""
attach("params.sage")


"""
Load G2 arithmetic
"""
attach("ecn2.sage")



"""
Load pairing arithmetic
"""
attach("ep.sage")



print "################################################################"
print "# Test Pairing arithmetic"
print "################################################################"



print "\nl_T,T(P)"
for i in [1..NTEST2]:
	P = G1_rand()
	Q = G2_rand()
	Q0 = G2_copyJ(Q)
	l, R = pointDblLineEval(Q, P)
	S = [Fp2_mul(R[0], Fp2_inv(R[2])), Fp2_mul(R[1], Fp2_inv(R[2])), Fp2_one()]
	T = G2_norm(G2_dblJ(Q0))
	#print S
	#print T
	diff = G2_addJJJ(S, G2_negJ(T))
	if diff != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0




print "\nl_T,Q(P)"
for i in [1..NTEST2]:
	P = G1_rand()
	T = G2_rand()
	Q = G2_rand()
	Q0 = G2_copyJ(T)
	l, R = pointAddLineEval(T, Q, P)
	S1 = [Fp2_mul(R[0], Fp2_inv(R[2])), Fp2_mul(R[1], Fp2_inv(R[2])), Fp2_one()]
	S2 = G2_norm(G2_addJJJ(Q0, Q))
	#print S1
	#print S2
	diff = G2_addJJJ(S1, G2_negJ(S2))
	if diff != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0


print "\nf . l (sparse)"
for i in [1..NTEST2]:
	f0 = rand12()
	P = G1_rand()
	Q = G2_rand()
	l, R = pointDblLineEval(Q, P)
	f1 = Fp12_mulFp2_024(f0, l)
	g  = [[l[0], Fp2_zero(), l[2]],[Fp2_zero(), l[1], Fp2_zero()]]
	f2 = Fp12_mul(f0, g)
	diff = Fp12_sub(f1, f2)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nf . l . l2 (sparse)"
for i in [1..NTEST2]:
	f0 = rand12()
	P = G1_rand()
	Q = G2_rand()
	T = G2_rand()
	l, T  = pointAddLineEval(T, Q, P)
	l2, T = pointAddLineEval(T, Q, P)
	f1 = Fp12_mulFp2_024_Fp2_024(l, l2)
	f1 = Fp12_mul(f0, f1)
	g  = [[l[0], Fp2_zero(), l[2]],[Fp2_zero(), l[1], Fp2_zero()]]
	f2 = Fp12_mul(f0, g)
	g  = [[l2[0], Fp2_zero(), l2[2]],[Fp2_zero(), l2[1], Fp2_zero()]]
	f2 = Fp12_mul(f2, g)
	diff = Fp12_sub(f1, f2)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0





print "\n\\varpsi(.)"
for i in [1..NTEST2]:
	Q = G2_rand()
	Q1 =p_power_FrobeniusJi(Q,randint(1,6))

	#check if Q1 \in E/Fpk
	X0 = [[Fp2_zero(),Q1[0],Fp2_zero()],Fp6_zero()]
	Y0 = [Fp6_zero(),[Fp2_zero(),Q1[1],Fp2_zero()]]
	XX = Fp12_add(Fp12_exp(X0,3),[[[B,0],Fp2_zero(),Fp2_zero()],Fp6_zero()])
	YY = Fp12_exp(Y0,2)
	diff = Fp12_sub(XX, YY)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nf^p"
for i in [1..1]:
	a = rand12()
	b = Fp12_exp(a, p)
	c = Fp12_powq(a, X)
	diff = Fp12_sub(b, c)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nf^p^6"
for i in [1..1]:
	a = rand12()
	b = a[:2]
	c = a[:2]
	for j in range(0,6):
		b = Fp12_exp(b, p)
		c = Fp12_powq(c, X)
	diff = Fp12_sub(b, c)
	if diff != Fp12_zero():
		fpe += 1
		#print diff
#	diff = Fp12_sub(a, Fp12_conj(c))
#	if diff != Fp12_zero():
#		fpe += 1
#		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nCyclotomic"
for i in [1..NTEST2]:
	a = rand12()
	a = EasyExpo(a)
	b = Fp12_inv(a)
	c = Fp12_conj(a)
	diff = Fp12_sub(b, c)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nFrobenius"
for i in [1..1]:
	a = rand12()
	a = EasyExpo(a)
	n = randint(1,12)
	b = Frobenius(a, n)
	d = a[:2]
	for j in range(0,n):
		d = Fp12_exp(d, p)
	diff = Fp12_sub(b, d)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nG_T order"
for i in [1..NTEST2//2]:
	a = rand12()
	a = EasyExpo(a)
	#b = HardExpo_Scottetal(a)
	b = HardExpo_Fuentesetal(a)
	diff = Fp12_sub(b, d)
	d = Fp12_exp(b, q)
	if d != Fp12_one():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nPairing order"
for i in [1..NTEST2//2]:
	P = G1_rand()
	Q = G2_rand()
	f = oap(P, Q)
	g = Fp12_exp(f, q)
	if g != Fp12_one():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nBilinearity"
for i in [1..NTEST2//2]:
	P = G1_rand()
	Q = G2_rand()
	n = rand()
	nP = G1_norm(G1_mulGLVwNAF(n,G1_toJ(P)))
	nQ = G2_norm(G2_mulGSwNAF(n,Q,WB,BB))
	f = oap(nP, Q)
	g = oap(P, nQ)
	diff = Fp12_sub(f, g)
	if diff != Fp12_zero():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0

