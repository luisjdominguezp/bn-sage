from hashlib import sha256

"""
EC arithmetic
"""



#H1 function
def H1(M):
	t0 = int(sha256(M).hexdigest(),16)%p

	return t0



#non-deterministic
def hash_and_mapE(ID):
	global B
	x0 = H1(ID)

	while 1:
		x3 = Fp_square(x0)
		x3 = Fp_mulC(x3, x0)
		x3 = Fp_addC(x3, B) #x^3+2
		y0 = Fp_SQRTp3m4(x3, 1)
		if y0 == -1:
			x0 +=1
			#print "."
		else:
			break

	return [x0, y0]



#non-deterministic
def G1_rand():
	ID = str(rand())
	P = hash_and_mapE(ID)

	return P



#deterministic
def hash_and_mapEDet(ID):
	global B
	global SQRTm3
	global c0

	t = H1(ID)
	w = Fp_mulC(t, t)
	w = Fp_addC(w, B+1)
	w = Fp_inv(w)
	w = Fp_mulC(w, t)
	w = Fp_mulC(w, SQRTm3)

	x    = [0, 0, 0]
	x[0] = Fp_mulC(w, t)
	x[0] = Fp_subC(x[0], c0)
	x[0] = Fp_negC(x[0])

	x[1] = Fp_addC(x[0], 1)
	x[1] = Fp_negC(x[1])

	x[2] = Fp_square(w)
	x[2] = Fp_inv(x[2])
	x[2] = Fp_addC(x[2], 1)

	a    = [0, 0, 0]
	for i in range(0, 3):
		a[i] = Fp_square(x[i])
		a[i] = Fp_mulC(a[i], x[i])
		a[i] = Fp_addC(a[i], B)

	alpha    = [0, 0]
	alpha[0] = Legendre(a[0], p)
	alpha[1] = Legendre(a[1], p)

	ii  = alpha[0] - 1
	ii *= alpha[1]
	ii %= 3

	Q0  = x[ii]
	c   = Legendre(t, p)
	d   = Fp_SQRTp3m4(a[ii], 0)

	if c<0:
		Q1 = Fp_negC(d)
	else:
		Q1 = d


	return [Q0, Q1]



#Deterministic
def G1_randDet():
	ID = str(rand())
	P = hash_and_mapEDet(ID)

	return P



# J <- J + J
def G1_addJJJ(P, Q):
	R = G1_zeroJ()

	if P == G1_zeroJ():
		return G1_copyJ(Q)

	if Q == G1_zeroJ():
		return G1_copyJ(P)

	Z1Z1 = Fp_square(P[2])
	Z2Z2 = Fp_square(Q[2])
	U1 = Fp_mulC(P[0], Z2Z2)
	U2 = Fp_mulC(Q[0], Z1Z1)
	t0 = Fp_mulC(Q[2], Z2Z2)
	S1 = Fp_mulC(P[1], t0)
	t1 = Fp_mulC(P[2], Z1Z1)
	S2 = Fp_mulC(Q[1], t1)
	H  = Fp_subC(U2, U1)
	t3 = Fp_subC(S2, S1)

	if H == Fp_zero():
		if t3 == Fp_zero():
			R = G1_dblJ(P)
		#else:
			# R = G1_zeroJ()

		return R

	t2 = Fp_addC(H, H)
	I  = Fp_square(t2)
	J  = Fp_mulC(H, I)
	r  = Fp_addC(t3, t3)
	V  = Fp_mulC(U1, I)
	t4 = Fp_square(r)
	t5 = Fp_addC(V, V)
	t6 = Fp_subC(t4, J)
	R[0] = Fp_subC(t6, t5)
	t7 = Fp_subC(V, R[0])
	t8 = Fp_mulC(S1, J)
	t9 = Fp_addC(t8, t8)
	t10  = Fp_mulC(r, t7)
	R[1] = Fp_subC(t10, t9)
	t11  = Fp_addC(P[2], Q[2])
	t12  = Fp_square(t11)
	t13  = Fp_subC(t12, Z1Z1)
	t14  = Fp_subC(t13, Z2Z2)
	R[2] = Fp_mulC(t14, H)

	return R



# J <- J
def G1_dblJ(P):
	R = G1_zeroJ()

	if P == G1_zeroJ():
		return R

	A  = Fp_square(P[0])
	B  = Fp_square(P[1])
	C  = Fp_square(B)
	t0 = Fp_addC(P[0], B)
	t1 = Fp_square(t0)
	t2 = Fp_subC(t1, A)
	t3 = Fp_subC(t2, C)
	D  = Fp_addC(t3, t3)
	E  = Fp_addC(A, A)
	E  = Fp_addC(E, A)
	F  = Fp_square(E)
	t4 = Fp_addC(D, D)
	R[0] = Fp_subC(F, t4)
	t5 = Fp_subC(D, R[0])
	t6 = C
	t6 = Fp_addC(t6, t6)#t6+= t6
	t6 = Fp_addC(t6, t6)#t6+= t6
	t6 = Fp_addC(t6, t6)
	#t6+= t6
	#t6 = t6 % p
	t7 = Fp_mulC(E, t5)
	t8 = Fp_mulC(P[1], P[2])
	R[1] = Fp_subC(t7, t6)
	R[2] = Fp_addC(t8, t8)

	return R



# J <- J + A
def G1_addJJA(P, Q):
	R = G1_zeroJ()
	if P == G1_zeroJ():
		if Q == G1_zeroA():
			return R #G1_zeroJ()
		else:
			return G1_toJ(G1_copyA(Q))
	if Q == G1_zeroA():
		return G1_copyJ(P)

	t1 = Fp_square(P[2])
	t2 = Fp_mulC(t1, P[2])
	t1 = Fp_mulC(t1, Q[0])
	t2 = Fp_mulC(t2, Q[1])
	t1 = Fp_subC(t1, P[0])
	t2 = Fp_subC(t2, P[1])

	if t1 == Fp_zero():
		if t2 != Fp_zero():
			return R#G1_zeroJ()
		else:
			R = G1_toJ(G1_copyA(Q))
			return G1_dblJ(R, p)

	R[2] = Fp_mulC(P[2], t1)
	t3   = Fp_square(t1)
	t4   = Fp_mulC(t3, t1)
	t3   = Fp_mulC(t3, P[0])
	t1   = Fp_addC(t3, t3)
	R[0] = Fp_square(t2)
	R[0] = Fp_subC(R[0], t1)
	R[0] = Fp_subC(R[0], t4)
	t3   = Fp_subC(t3, R[0])
	t3   = Fp_mulC(t3, t2)
	t4   = Fp_mulC(t4, P[1])
	R[1] = Fp_subC(t3, t4)

	return R



# Double-and-Add scalar-point multiplication
# J <- J
def G1_mulDA(k, P):
	#R = G1_zeroJ()
	if k == 0:
		return G1_zeroJ()
	if k == 1:
		return P
	if k == 2:
		R = G1_dblJ(P)
		return R

	lbin = k.nbits()
	bin_k= k.bits()
	R = G1_copyJ(P)
	for i in range(lbin-2,-1,-1):
		R = G1_dblJ(R)
		if bin_k[i] == 1:
			R = G1_addJJJ(R, P) #JJA

	return R



# Precompute for wNAF, w=3
def G1_precomputeDA3NAF(P):
	Pi = [G1_zeroJ() for i in range (2)]
	Pi[0] = G1_copyJ(P)
	Pi[1] = G1_dblJ(Pi[0])
	Pi[1] = G1_addJJA(Pi[1], Pi[0])
	Pi[1] = G1_norm(Pi[1])

	return Pi



# Double-and-Add scalar-point multiplication with wNAF
# J <- J
def G1_mulDAwNAF(k, P):
	#global WNAF
	#R = G1_zeroJ()
	if k == 0:
		return G1_zeroJ()
	if k == 1:
		return P
	if k == 2:
		R = G1_dblJ(P)
		return R

	mp, mn, tlen = NAFw64(k, 3)

	if (P[2] != Fp_one()):
		P = G1_norm(P)
	Pi = G1_precomputeDA3NAF(G1_toJ(P))
	R = G1_zeroJ()
	for i in range(tlen-1,-1,-1):
		R = G1_dblJ(R)
		if mp[i] != 0:
			R = G1_addJJA(R, Pi[mp[i]>>1])
		if mn[i] != 0:
			R = G1_addJJA(R, G1_negJ(Pi[mn[i]>>1]))

	return R



# GLV method, GS-like decomposition
# J <- J
def G1_mulGLVwNAF(k, P):
	#global WNAF
	#R = G1_zeroJ()
	if k == 0:
		return G1_zeroJ()
	if k == 1:
		return P
	if k == 2:
		R = G1_dblJ(P)
		return R

	u = G1Decomp(n, W, SB)
	mp = [[0 for j in range(0, 130)] for i in range(0, 2)]
	mn = [[0 for j in range(0, 130)] for i in range(0, 2)]
	tlen = [0 for i in range(0,  2)]

	for i in range(0, 2):
		mp[i], mn[i], tlen[i] = NAFw(u[i].abs(), 3)

	tlent = tlen[0]
	if tlen[1] > tlen[0]:
		tlent = tlen[1]

	Pi = [[G1_zeroJ() for i in range(0, 2)] for j in range (0, 2)]

	if (P[2] != Fp_one()):
		P = G1_norm(P)
	P0 = G1_copyJ(P)
	if u[0] < 0:
		P0 = G1_negJ(P0)
	Q = [Fp_mulC(bbeta, P[0]), P[1], P[2]]
	if u[1] < 0:
		Q  = G1_negJ(Q)

	Pi[0] = G1_precomputeDA3NAF(P)
	Pi[1] = G1_precomputeDA3NAF(Q)

	R = G1_zeroJ()
	for i in range(tlent-1,-1,-1):
		R = G1_dblJ(R)
		for j in range(0,2):
			if mp[j][i] != 0:
				R = G1_addJJA(R, Pi[j][mp[j][i]>>1])
			if mn[j][i] != 0:
				R = G1_addJJA(R, G1_negJ(Pi[j][mn[j][i]>>1]))

	return R



# Point-at-infinity in Jacobians
def G1_zeroJ():

	return [Fp_one(), Fp_one(), Fp_zero()]



# Point-at-infinity in Affine
def G1_zeroA():

	return [Fp_zero(), Fp_one()]



# -P in Jacobians
def G1_negJ(P):

	return [P[0], Fp_negC(P[1]), P[2]]



# -P in Affine
def G1_negA(P):

	return [P[0], Fp_negC(P[1])]



# Safe point copy in Jacobians
def G1_copyJ(P):
	R = G1_zeroJ()
	R[0] = P[0]
	R[1] = P[1]
	R[2] = P[2]

	return R



# Safe point copy in Affine
def G1_copyA(P):
	R = G1_zeroA()
	R[0] = P[0]
	R[1] = P[1]

	return R



# Normalisation of point
def G1_norm(P):
	R = G1_zeroJ()
	if P[2] == Fp_zero():
		return G1_zeroJ()

	if P[2] == Fp_one():
		return G1_copyJ(P)

	Z = P[2]
	Z = Fp_inv(Z)
	Z2 = Fp_square(Z)
	Z3 = Fp_mulC(Z2, Z)
	R[0] = Fp_mulC(P[0], Z2)
	R[1] = Fp_mulC(P[1], Z3)
	R[2] = Fp_one()

	return R



# J <- A
def G1_toJ(P):
	R = G1_zeroJ()
	R[0] = P[0]
	R[1] = P[1]
	R[2] = Fp_one()

	return R



# wNAF decomposition
# At lower level it made sense to implement deparate functions
def NAFw(k, w):
	if k.nbits() > 64:
		return NAFw128(k, w)
	else:
		return NAFw64 (k, w)



# wNAF decomposition for 2 WORDs, see if it could be merged with NAFw64
def NAFw128(k, w):
	l = k
	mp = [0 for i in range (0,130)]
	mn = [0 for i in range (0,130)]

	twotoW   = 2^w
	twotoWm1 = 2^(w-1)
	mask     = twotoW-1

	tlen = 0
	i = 0

	while (l>0):
		if l&1:
			temp = l & mask

			if (temp < twotoWm1):
				mp[i] = temp
				l -= mp[i]

			else:
				mn[i] = twotoW - temp
				l += mn[i]

		l  >>= 1
		i   += 1
		tlen+= 1

	return mp, mn, tlen



# wNAF decomposition for 1 WORD, see if it could be merged with NAFw128
def NAFw64(k, w):
	l = k
	mp = [0 for i in range (0,70)]
	mn = [0 for i in range (0,70)]

	twotoW   = 2^w
	twotoWm1 = 2^(w-1)
	mask     = twotoW-1

	tlen = 0
	i = 0

	while (l>0):
		if l&1:
			temp = l & mask

			if (temp < twotoWm1):
				mp[i] = temp
				l -= mp[i]

			else:
				mn[i] = twotoW - temp
				l += mn[i]

		l  >>= 1
		i   += 1
		tlen+= 1

	return mp, mn, tlen



# wNAF decomposition for 2 WORDs, see if it could be merged with NAFw64
def NAFw2(k):
	l   = k
	n   = k.nbits() + 1
	naf = [0 for i in range (0,n)]
	w   = 2

	twotoW   = 2^w
	twotoWm1 = 2^(w-1)
	mask     = twotoW-1

	tlen = 0
	i = 0

	while (l>0):
		if l&1:
			temp = 2 - (l & mask)
			naf[i] = temp
			l -= naf[i]

		l  >>= 1
		i   += 1
		tlen+= 1

	return naf, tlen



#Sanchez-Ramirez trick for ``mad'' division
def notsomadSR(WB, k):
	v0 = WB*k
	v1 = v0.abs()
	v1 >>= 256

	if v0 < 1:
		v1 *= -1

	return v1



# GS-like decomposition for GLV multiplication
def G1Decomp(k, W, BB):
	v0 = [0,0]
	u  = [0,0]
	v0[0] = notsomadSR(W[0], k)
	v0[1] = notsomadSR(W[1], k)
	u[0]  = k
	u[1]  = 0

	for i in range(0,2):
		for j in range(0,2):
			u[i] = u[i] - v0[j] * BB[j][i]

	return u



#simultaneous normalisation using Montgomery's trick
# A <- J
def MultiNormalizeFp(PJ, n):
	PN = [G1_zeroA() for i in range(0, n)]
	if n == 0:
		return PN
	if n == 1:
		P0 = G1_norm(PJ[0])
		PN[0][0] = P0[0]
		PN[0][1] = P0[1]
		return P

	t = [1 for i in range(0, 2*n)]
	d = PJ[0][2]

	#(n-1)*(m)
	for i in range(1, n):
		d = Fp_mulC(d, PJ[i][2])
	d = Fp_inv(d)
	t[1]     = PJ[0][2]
	t[n + 1] = PJ[n-1][2]

	#(n-2)*(2*m)
	for i in range(2, n):
		t[i]     = Fp_mulC(t[i - 1], PJ[i - 1][2])
		t[n + i] = Fp_mulC(t[n + i - 1], PJ[n - i][2])

	#(n-2)*(2*m)
	for i in range(1, n-1):
		t[i] = Fp_mulC(t[i], t[2 * n - i - 1])
	t[0] = t[2 * n - 1]

	# (n) * (4*m + s)
	for i in range(0, n):
		t0 = Fp_mulC(d, t[i])
		d2 = Fp_square(t0)
		d3 = Fp_mulC(d2, t0)
		PN[i][0] = Fp_mulC(PJ[i][0], d2)
		PN[i][1] = Fp_mulC(PJ[i][1], d3)
		#PN[i][2] = 1


	return PN


def G1_precomputeSC(P):
	t = 256
	d = 32
	PQ = [G1_zeroJ() for i in range (0, t)]
	PP = [G1_zeroJ() for i in range (0, t//2)]

	PQ[0] = [1, 1, 1] #for normalisation
	PQ[1] = G1_copyJ(G1_toJ(P))

	td = 2^d
	PQ[2] = G1_mulDA(td, PQ[1])
	pp1 = 2
	for i in range(3, t):
		if i==(1<<pp1):
			PQ[i] = G1_mulDA(td, PQ[(1<<(pp1-1))])
			pp1 += 1
		else:
			PQ[i] = PQ[(1<<(pp1-1))]
			pp2 = i
			log2i = i.bit_length()#int(log(i,2))#i.bit_length() ?
			for j in range (0, log2i-1):
				if pp2&1 == 1:
					PQ[i] = G1_addJJJ(PQ[i], PQ[(1<<j)])
				pp2 >>= 1

	for i in range(0, t//2):
		PQ[i] = G1_addJJJ(PQ[i], G1_negJ(PQ[t-1-i]))

	PP = MultiNormalizeFp(PQ, t//2)
	PP[0] = G1_negJ(G1_norm(PQ[255]))

	return PP



def RecodeForPrecompC(k, PT):
	out  = [0 for i in range(0, 32)]
	acum = [0 for i in range(0,  4)]
	t0   = [[0 for j in range(0, 4)] for i in range(8)]

	l = k

	for i in range(0, 8):
		for j in range(0, 4):
			t0[i][j] = l&0xFF
			l >>= 8

	for j in range(0, 4):
		acum[j] = 0
		for i in range(0, 8):
			acum[j] ^^= PT[i][t0[i][j]] #watch out the double ^^

	for i in range(0, 32):
		out[i] = acum[i // 8]&0xFF
		acum[i // 8] >>= 8


	return out



def RecodeForPrecompSC(k, PT, t256, Half_Zr):
	#l = k
	l = (k+t256-1) * Half_Zr
	l = l % q
	out = RecodeForPrecompC(l, PT)

	return out



def G1_mulSC(k, PP, PT, t256, Half_q):
	R = G1_zeroJ()
	Q = G1_zeroA()
	acum = RecodeForPrecompSC(k, PT, t256, Half_q)

	for i in range(31,-1,-1):
		R = G1_dblJ(R)
		if ((acum[i]&0x80) == 0x80):
			Q[0] = PP[acum[i]^^0xFF][0]
			Q[1] = PP[acum[i]^^0xFF][1]
			Q = G1_negA(Q)
			R = G1_addJJA(R, Q)
		else:
			Q[0] = PP[acum[i]][0] #|0x00
			Q[1] = PP[acum[i]][1]
			Q = G1_copyA(Q)
			R = G1_addJJA(R, Q)

	return R
