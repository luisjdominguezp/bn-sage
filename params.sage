# This parameter generates a GT-strong subgroup, but has a heavy Hamming weight, any other recommendation?
#z = -(2^62 + 2^47 + 2^38 + 2^37 + 2^14 + 2^7 + 2^0)
z = -(2^62+2^55+1)
print(z)
print(ceil(log(abs(6*z + 2),2)))
print(ceil(log(abs(z),2)))

#Parameters of the curve
Zx.<x> = PolynomialRing(QQ)
k = 12
px  = 36*x^4 + 36*x^3 + 24*x^2 + 6*x + 1
qx  = 36*x^4 + 36*x^3 + 18*x^2 + 6*x + 1
tx  = 6*x^2 + 1
p   = Integer(px(z))
q	= Integer(qx(z))
t	= Integer(tx(z))
Fp  = GF(p) # For random number generator
beta = -1
chi = [1,1] #affects pointDblLineEval_woP step 4, but check workaround
#gamma = ?
beta3 = -1


# Barret reduction
B_W = 64
B_b = 2^B_W
B_k = Integer(math.ceil(math.log(p,B_b)+1))
B_mu = B_b^(2*B_k) // p
B_mask = 2^(B_W*(B_k+1))-1
B_expo = B_b^(B_k + 1)


#Curve form
A   = 0
B   = 2

#E2 form
invChi   = Fp2_inv(chi)
B_invChi = Fp2_mul([B, 0], invChi) #Fp2_mul_Fp (?)


#This w-NAF is better for BN curves
WNAF = 3

#SQRT constant
pp1o4 = Fp_mulC((p+1), Fp_inv(4))
pm3o4 = Fp_mulC((p-3), Fp_inv(4))

# \lambda^2 + \lambda + 1
bbetax   = -(18*x^3 + 18*x*x + 9*x + 2)
bbeta    = Integer(bbetax(z))
llambdax = -(36*x^3 + 18*x^2 + 6*x + 2)
llambda  = Integer(llambdax(z))


# GLV & GS matrices
W  = [ x for i in range(1,3)]
WB = [ x for i in range(1,5)]
SB = [[x for i in range(1,3)] for j in range (1,3)]
BB = [[x for i in range(1,5)] for j in range (1,5)]

W[0] =   6 * z * z + 4 * z + 1
W[1] = -(2 * z + 1)

SB[0][0] =   6 * z * z + 2 * z
SB[0][1] = -(2 * z + 1)
SB[1][0] = -(2 * z + 1)
SB[1][1] = -(6 * z * z + 4 * z + 1)


WB[0] = -(6 * z * z + 6 * z + 2)
WB[1] =  -1
WB[2] =   z
WB[3] =   1 + 3 * z + 6 * z * z + 6 * z * z * z

BB[0][0] = 2 * z
BB[0][1] = 1 + z
BB[0][2] =   - z
BB[0][3] =     z

BB[1][0] =  1 +     z
BB[1][1] = -1 - 3 * z
BB[1][2] = -1 +     z
BB[1][3] =  1

BB[2][0] = -1
BB[2][1] =  2 + 6 * z
BB[2][2] =  2
BB[2][3] = -1

BB[3][0] =  2 + 6 * z
BB[3][1] =  1
BB[3][2] = -1
BB[3][3] =  1


#Sanchez-Ramirez trick for fast GLV/GS decomposition
for i in range(0,2):
	a = W[i].abs()
	a <<= 256
	a //= q
	if W[i] < 0:
		a *= -1
	W[i] = a

for i in range(0,4):
	a = WB[i].abs()
	a <<= 256
	a //= q
	if WB[i] < 0:
		a *= -1
	WB[i] = a


#Constants for deterministic hashing into E, and E'
Half   = Fp_inv(2)
Half_q = Fp_inv(2, q)
t256   = (1 << 256)
SQRTm3 = Fp_SQRTp3m4(p-3, 0)
c0     = Fp_mulC(Fp_addC(p-1,  SQRTm3), Half)

#PT is in another file
load('PTgen.sage')
PT = PTgen()

V = [Fp2_zero(), Fp2_zero(), Fp2_zero(), Fp2_zero()]
V[0] = Fp2_exp(chi, (p-1)//3)
V[1] = Fp2_exp(chi, (p-1)//2)
V[2] = Fp2_inv(V[0])
V[3] = Fp2_inv(V[1])
#V[0] = Fp2_exp(V[0], 2)
#V[0] = Fp2_exp(V[1], 3)

X = Fp2_exp(chi, (p-1)//6)



#siTbl,_ = (NAFw2(abs(6*z+2))).reverse
siTbl = (abs(6*z+2).bits())
#siTbl.reverse()

#Montgomery space
M_r = 1 << 256
M_pp = -(Fp_inv(p, M_r))
M_rp = Fp_inv(M_r, p)
