"""
Fp arithmetic
"""

global SAGE_COUNT_OP
global fpa
global fpm
global fpi
global fpmb
global fpe

try:
	SAGE_COUNT_OP
except NameError:
	fpa  = 0
	fpm  = 0
	fpi  = 0
	fpmb = 0
	SAGE_COUNT_OP = 1
	fpe = 0



#Input : a, b \in F_p
#Output: a+b \in F_p
def Fp_addC(a,b):
	c = Fp_addNR(a, b)

	if c >= p:
		c = Fp_subNR(c, p)

	return c



#Input : a, b \in Z
#Output: a+b
def Fp_addNR(a,b):
	global SAGE_COUNT_OP
	global fpa

	c = a + b

	if SAGE_COUNT_OP == 1:
		fpa = fpa + 1

	return c



#Input : a, b \in F_p
#Output: a-b \in F_p
def Fp_subC(a,b):
	c = Fp_subNR(a, b)
	if a < b:
		c = Fp_addNR(c, p)

	return c



#Input : a, b \in F_p
#Output: a-b \in Z
def Fp_subNR(a,b):
	global SAGE_COUNT_OP
	global fpa

	c = a - b

	if SAGE_COUNT_OP == 1:
		fpa = fpa + 1

	return c



#rs = (z & B_mask) - ((qh * p) & B_mask)
#Input : a \in Z
#Output: a \in F_p
def Fp_BarretRed(z):
	qh = ((z >> (B_W * (B_k - 1))) * B_mu) >> (B_W * (B_k + 1))
	rs = Fp_subNR(Fp_mod(z, B_expo), Fp_mod(Fp_mulNR(qh, p), B_expo))

	if rs < 0:
		rs = Fp_addNR(rs, B_expo)
	while (rs >= p):
 		rs = Fp_subNR(rs, p)


	return rs



#Input : a, b \in F_p
#Output: a.b \in F_p
def Fp_mulC(a,b):
	c = Fp_BarretRed(Fp_mulNR(a, b))

	return c



#Input : a \in F_p
#Output: a^2 \in F_p
def Fp_square(a):
	c = Fp_mulC(a,a)

	return c



#Input : a, b \in Z
#Output: a.b \in Z
def Fp_mulNR(a,b):
	global SAGE_COUNT_OP
	global fpm

	c = (a * b)

	if SAGE_COUNT_OP == 1:
		fpm = fpm + 1

	return c



#Input : a \in F_p
#Output: -a \in F_p
def Fp_negC(a):
	c = Fp_subC(0,a)

	return c



#Input : a \in F_p
#Output: a^-1 \in F_p
def Fp_invBin(a, q=None):
	if q==None:
		n = p
	else:
		n = q

	u  = a
	v  = n
	x1 = 1
	x2 = 0
	while (u != 1) and (v != 1):
		while (u&1) == 0:
			u = Fp_divBy2(u)
			if (x1&1)==0:
				x1 = Fp_divBy2(x1)
			else:
				x1 = Fp_divBy2(Fp_addNR(x1, n))
		while (v&1)==0:
			v = Fp_divBy2(v)
			if (x2&1)==0:
				x2 = Fp_divBy2(x2)
			else:
				x2 = Fp_divBy2(Fp_addNR(x2, n))
		if u >= v:
			u  = Fp_subNR(u, v)
			x1 = Fp_subNR(x1, x2)
		else:
			v  = Fp_subNR(v, u)
			x2 = Fp_subNR(x2, x1)

	if u == 1:
		return Fp_BarretRed(x1)
	else:
		return Fp_BarretRed(x2)



#Input: a
#Output: a//2
def Fp_divBy2(a):
	if a > 2:
		return a >> 1
	else:
		return a//2
	#return a//2



def Fp_inv(a, q=None):
	global SAGE_COUNT_OP
	global fpi
	if q==None:
		n = p
	else:
		n = q

	v = [0,0]
	v[0] = a
	v[1] = n
	x = [0,0]
	x[0] = 1
	x[1] = 0
	i =1
	while v[i] != 0:
		i    = (i + 1) & 1
		j    = (i + 1) & 1
		q    = Integer(floor(v[i] / v[j]))
		v[i] = v[i] - (q * v[j])
		x[i] = x[i] - (q * x[j])

	i = (i + 1) & 1

	if SAGE_COUNT_OP == 1:
		fpi = fpi + 1

	return x[i] % n



#is this cheaper than a string comparison? (fails for large-ish values)
binary = lambda n: n>0 and [n&1]+binary(n>>1) or []



#Input : f, e \in F_p
#Output: f^e \in F_p
def Fp_exp(f, e):
	if e == 0:
		return 1
	if e == 1:
		return f
	if e == 2:
		g = Fp_square(f)
		return g

	lbin = e.nbits()
	bin_e= e.bits()
	g = f
	for i in range(lbin-2,-1,-1):
		g = Fp_square(g)
		if bin_e[i] == 1:
			g = Fp_mulC(g, f)

	return g



#Input : a \in F_p, for p=3%4
#Output: a^(1/2) \in F_p
def Fp_SQRTp3m4(f, flag=0):
	global pp1o4
	if f==4:
		return 2

	g = Fp_exp(f,pp1o4)

	if flag==1:
		g2 = Fp_square(g)
		if g2 != f:
			return -1

	return g



#Input : a \in F_p, for p=3%4
#Output: a^(1/2) \in F_p
def Fp_SQRTshanks(f):
	global pm3o4
	g = Fp_exp(f,pm3o4)
	a = Fp_mulC(Fp_square(g), f)
	g = Fp_mulC(g, f)
	if a == (p-1):
		return -1

	return g



#Input: a,b \in Mont(F_p)
#Output: a.b \in Mont(F_p)
def MontPr(a, b):
	t = Fp_mulNR(a,b)
	q = Fp_mod(Fp_mulNR(t, M_pp), M_r)
	u = Fp_addNR(t, Fp_mulNR(q, p)) >> 256
	if u >= p:
		return Fp_subNR(u, p)
	else:
		return u

	return u



#Input: a,b \in F_p
#Output: a.b \in F_p
def Fp_mulM(a,b):
	ap = Fp_mod(a<<256, p)
	bp = Fp_mod(b<<256, p)
	cp = MontPr(ap, bp)
	c  = MontPr(cp, Fp_one())

	return c



#Input: a,b \in F_p
#Output: a.b \in F_p
def Fp_mulMI(a,b):
	ap = Fp_mod(a<<256, p)
	c = MontPr(ap, b)

	return c



#Input: a,e \in F_p
#Output: a^e \in F_p
def Fp_expM(a,e):
	ap = Fp_mod(a<<256, p)
	aa = Fp_mod(M_r, p)

	lbin = e.nbits()
	bin_e= e.bits()
	for i in range(lbin-1,-1,-1):
		aa = MontPr(aa, aa)
		if bin_e[i]==1:
			aa = MontPr(aa, ap)

	return MontPr(aa, Fp_one())



def Fp_zero():
	A = 0

	return A



def Fp_one():
	A = 1

	return A


def Fp_copy(v):
	u = v.abs()

	return u


def rand():
	A = Integer(Fp.random_element())

	return A



def Fp_mod(a, b):
	return a%b

#Legendre symbol
def Legendre(a, p):
	k = 1
	A = a
	B = p

	while B != 1:
		if A == 0:
			return 0

		v = 0
		while A&1 == 0:
			v  += 1
			A >>= 1

		t0 = B&7

		#keep the parenthesis
		if ((v&1) == 1) & ((t0 == 3) | (t0 == 5)):
			k = -k

		#idem
		if ((A&3) == 3) & ((t0&3) == 3):
			k = -k

		r = A
		A = B % r
		B = r

	if B > 1:
		k = 0

	return k
