attach("fp.sage")

"""
Fp3 arithmetic
Fp2: Fp[w]/w^3 - beta
beta = -1
"""



def beta3_mul(a):
	global SAGE_COUNT_OP
	global fpm

	if beta3==1:
			c = a
	if beta3==-1:
			c = p-a
	if beta3<1:
		c = Fp_mulC(a,beta3)
		#c = Fp_negC(a)
	if beta>1:
		c = Fp_mulC(a,beta3)


	return c



#Input : a, b \in F_{p^3} -> a = (a0 + a1w + a2w^2),
# b = (b0 + b1w + b2w^2)
#Output: c = a + b \in F_{p^3}
def Fp3_addC(a,b):
	c0 = Fp_addC(a[0], b[0])
	c1 = Fp_addC(a[1], b[1])
	c2 = Fp_addC(a[2], b[2])

	return [c0, c1, c2]



def Fp3_addNR(a,b):
	global SAGE_COUNT_OP
	global fpa

	c0 = a[0] + b[0]
	c1 = a[1] + b[1]
	c2 = a[2] + b[2]

	if SAGE_COUNT_OP == 1:
		fpa = fpa + 3

	return [c0, c1, c2]



#Input : a, b \in F_{p^3} -> a = (a0 + a1w + a2w^2),
# b = (b0 + b1w + b2w^2)
#Output: c = a + b \in F_{p^3}
def Fp3_subC(a,b):
	c0 = Fp_subC(a[0], b[0])
	c1 = Fp_subC(a[1], b[1])
	c2 = Fp_subC(a[2], b[2])

	return [c0, c1, c2]



#Input : a \in F_{p^3} -> a = (a0 + a1w + a2w^2)
#Output: -a \in F_{p^3}
def Fp3_neg(a):
	c0 = p - a[0]
	c1 = p - a[1]
	c2 = p - a[2]

	return [c0, c1, c2]



#Input : a \in F_{p^3}, b \in F_p -> a = (a0 + a1w + a2w^2),
# b = (b0 + b1w + b2w^2), p-prime
#Output: c = b.a \in F_{p^3}
def Fp3_mul_Fp(a, b):
	c0 = Fp_mulC(a[0], b)
	c1 = Fp_mulC(a[1], b)
	c2 = Fp_mulC(a[2], b)

	return [c0, c1, c2]



#Input : a, b \in F_{p^3} -> a = (a0 + a1w + a2w^2),
# b = (b0 + b1w + b2w^2)
#Output: c = a.b \in F_{p^3}
def Fp3_mul(a, b):
	v0 = Fp_mulC(a[0], b[0])
	v1 = Fp_mulC(a[1], b[1])
	v2 = Fp_mulC(a[2], b[2])
	c0 = Fp_mulC(Fp_addC(a[1], a[2]), Fp_addC(b[1], b[2]))
	c0 = Fp_subC(Fp_subC(c0, v1), v2)
	c0 = Fp_addC(beta3_mul(c0), v0)
	c1 = Fp_mulC(Fp_addC(a[0], a[1]), Fp_addC(b[0], b[1]))
	c1 = Fp_subC(Fp_subC(c1, v0), v1)
	c1 = Fp_addC(c1, beta3_mul(v2))
	c2 = Fp_mulC(Fp_addC(a[0], a[2]), Fp_addC(b[0], b[2]))
	c2 = Fp_subC(Fp_subC(c2, v0), v2)
	c2 = Fp_addC(c2, v1)

	return [c0, c1, c2]



#Input : a \in F_{p^3} -> a = (a0 + a1w + a2w^2)
#Output: a^2 \in F_{p^3}
def Fp3_square(a):
	v4 = Fp_mulC(a[0], a[1])
	v4 = Fp_addC(v4, v4)
	v5 = Fp_square(a[2])
	c1 = Fp_addC(beta3_mul(v5), v4)
	v2 = Fp_subC(v4, v5)
	v3 = Fp_square(a[0])
	v4 = Fp_addC(Fp_subC(a[0], a[1]), a[2])
	v5 = Fp_mulC(a[1], a[2])
	v5 = Fp_addC(v5, v5)
	v4 = Fp_square(v4)
	c0 = Fp_addC(beta3_mul(v5), v3)
	c2 = Fp_subC(Fp_addC(Fp_addC(v2, v4), v5),v3)

	return [c0, c1, c2]



#Input : a\in F_{p^3} -> a = (a0 + a1w + a2w^2)
#Output: a^-1 \in F_{p^3}
def Fp3_inv(a):
	v0 = Fp_square(a[0])
	v1 = Fp_square(a[1])
	v2 = Fp_square(a[2])
	v3 = Fp_mulC(a[0], a[1])
	v4 = Fp_mulC(a[0], a[2])
	v5 = Fp_mulC(a[1], a[2])
	A  = Fp_subC(v0, beta3_mul(v5))
	B  = Fp_subC(beta3_mul(v2), v3)
	C  = Fp_subC(v1, v4)
	v6 = Fp_mulC(a[0], A)
	v6 = Fp_addC(v6, Fp_mulC(beta3_mul(a[2]), B))
	v6 = Fp_addC(v6, Fp_mulC(beta3_mul(a[1]), C))
	F  = Fp_invBin(v6)
	c0 = Fp_mulC(A, F)
	c1 = Fp_mulC(B, F)
	c2 = Fp_mulC(C, F)

	return [c0, c1, c2]



def Fp3_exp(f, e):
	if e == 0:
		return 1
	if e == 1:
		return f
	if e == 2:
		g = Fp3_square(f)
		return g

	lbin = e.nbits()
	bin_e= e.bits()
	g = f[:3]
	for i in range(lbin-2,-1,-1):
		g = Fp3_square(g)
		if bin_e[i] == 1:
			g = Fp3_mul(g, f)

	return g



def Fp3_zero():
	A = [Fp_zero(), Fp_zero(), Fp_zero()]

	return A



def Fp3_one():
	A = [Fp_one(), Fp_zero(), Fp_zero()]

	return A


def Fp3_copy(A):
	B = Fp2_zero()
	B[0] = Fp_copy(A[0])
	B[1] = Fp_copy(A[1])
	B[2] = Fp_copy(A[2])

	return B



def rand3():
	A = [Integer(Fp.random_element()), Integer(Fp.random_element()), Integer(Fp.random_element())]
	return A
