attach("fp.sage")

"""
Fp2 arithmetic
Fp2: Fp[u]/u^2 - beta
beta = -1
"""



def beta_mul(a):
	global SAGE_COUNT_OP
	global fpm

	if beta==1:
			c = a
	if beta==-1:
			c = p-a
	if beta<1:
		c = Fp_mulC(a,beta)
		#c = Fp_negC(a)
	if beta>1:
		c = Fp_mulC(a,beta)


	return c



#Input : a, b \in F_{p^2} -> a = (a0 + a1u), b = (b0 + b1u)
#Output: c = a + b \in F_{p^2}
def Fp2_addC(a,b):
	c0 = Fp_addC(a[0], b[0])
	c1 = Fp_addC(a[1], b[1])

	return [c0, c1]



def Fp2_addNR(a,b):
	global SAGE_COUNT_OP
	global fpa

	c0 = a[0] + b[0]
	c1 = a[1] + b[1]

	if SAGE_COUNT_OP == 1:
		fpa = fpa + 2

	return [c0, c1]



def Fp2_subC(a,b):
	c0 = Fp_subC(a[0], b[0])
	c1 = Fp_subC(a[1], b[1])

	return [c0, c1]



def Fp2_neg(a):
	c0 = p - a[0]
	c1 = p - a[1]

	return [c0, c1]



def Fp2_conj(a):
	c0 = a[:1][0]
	c1 = p - a[1]

	return [c0, c1]



def Fp2_mul_Fp(a, b):
	c0 = Fp_mulC(a[0], b)
	c1 = Fp_mulC(a[1], b)

	return [c0, c1]



#Input : a, b \in F_{p^2} -> a = (a0 + a1u), b = (b0 + b1u)
#Output: a.b \in F_{p^2}
def Fp2_mul(a, b):
	v0 = Fp_mulC(a[0], b[0])
	v1 = Fp_mulC(a[1], b[1])
	c0 = beta_mul(v1)
	c0 = Fp_addC(v0, c0)
	c1 = Fp_addC(a[0], a[1])
	t0 = Fp_addC(b[0], b[1])
	c1 = Fp_mulC(c1, t0)
	c1 = Fp_subC(c1, v0)
	c1 = Fp_subC(c1, v1)

	return [c0, c1]



#Input : a \in F_{p^2} -> a = (a0 + a1u)
#Output: a^2 \in F_{p^2}
def Fp2_square(a):
	v0 = Fp_subC(a[0], a[1])
	v3 = beta_mul(a[1])
	v3 = Fp_subC(a[0], v3)
	v2 = Fp_mulC(a[0], a[1])
	v0 = Fp_mulC(v0, v3)
	v0 = Fp_addC(v0, v2)
	v1 = Fp_addC(v2, v2)
	v3 = beta_mul(v2)
	v0 = Fp_addC(v0, v3)

	return [v0, v1]



#Input : a \in F_{p^2} -> a = (a0 + a1u)
#Output: a^-1 \in F_{p^2}
def Fp2_inv(a):
	t0 = Fp_square(a[0])
	t1 = Fp_square(a[1])
	t1 = beta_mul(t1)
	t0 = Fp_subC(t0, t1)
	t1 = Fp_inv(t0)
	c0 = Fp_mulC(a[0], t1)
	c1 = Fp_mulC(Fp_negC(a[1]), t1)

	return [c0, c1]



def Fp2_exp(f, e):
	if e == 0:
		return 1
	if e == 1:
		return f
	if e == 2:
		g = Fp2_square(f)
		return g

	lbin = e.nbits()
	bin_e= e.bits()
	g = f[:2]
	for i in range(lbin-2,-1,-1):
		g = Fp2_square(g)
		if bin_e[i] == 1:
			g = Fp2_mul(g, f)

	return g



def Fp2_zero():
	A = [Fp_zero(), Fp_zero()]

	return A



def Fp2_one():
	A = [Fp_one(), Fp_zero()]

	return A


def Fp2_copy(A):
	B = Fp2_zero()
	B[0] = Fp_copy(A[0])
	B[1] = Fp_copy(A[1])

	return B



def rand2():
	A = [Integer(Fp.random_element()), Integer(Fp.random_element())]
	return A



#Input : a \in F_{p^2} -> a = (a0 + a1u)
#Output: a^(1/2) \in F_{p^2}
def Fp2_SQRT(v):
	u = Fp2_copy(v)
	if u[1] == Fp_zero():
		u[0] = Fp_SQRTshanks(u[0])
		return u

	t0 = Fp_square(u[0])
	t1 = Fp_square(u[1])
	t0 = Fp_subC(t0, beta_mul(t1))

	L = Legendre(t0, p)
	if L == -1:
		return [-1, 0]
	t0 = Fp_SQRTshanks(t0)

	t1 = Fp_addC(u[0], t0)
	t1 = Fp_mulC(t1, Half)
	L = Legendre(t1, p)
	if L == -1:
		t1 = Fp_subC(u[0], t0)
		t1 = Fp_mulC(t1, Half)

	u[0] = Fp_SQRTshanks(t1)
	t1   = Fp_addC(u[0], u[0])
	t1   = Fp_inv(t1)
	u[1] = Fp_mulC(u[1], t1)

	return u


def Fp2_mod(a, b):
	c    = Fp2_zero()
	c[0] = Fp_mod(a[0], b[0])
	c[1] = Fp_mod(a[1], b[1])

	return c
