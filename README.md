# README #

* Run sage

```
#!python
load ('TestFp.sage')
load ('TestG1.sage')
load ('TestG2.sage')
load ('TestPairing.sage')

```


### What is this repository for? ###

* This is the code for the Pairing Book
* Version 0.1



### How do I get set up? ###

* See the beginning of Test*.sage for sample start code



### Contribution guidelines ###

* This code contains ideas or algorithms from
    1. "High-Speed Software Implementation of the Optimal Ate Pairing over Barreto-Naehrig Curves" by Beuchat, González Díaz, Mitsunari, Okamoto, Rodríguez-Henríquez, and Teruya
    1. "Faster Explicit Formulas for Computing Pairings over Ordinary Curves" by Aranha, Karabina, Longa, Gebotys, and López
    1. Certivox MIRACL by Michael Scott
    1. The Software Implementation, and the Final Exponentiation Chapters from the Pairing Book
    1. Others that I can't remember (my thesis maybe?)


### Who do I talk to? ###

* Luis J. Dominguez Perez
* Diego Aranha, Peter Schwabe, and Amibe EL Mrabet