from math import *
from random import *

NTEST = 1000
NTEST2 = 50

"""
Op counters
"""
fpa  = 0
fpm  = 0
fpi  = 0
fpmb = 0

SAGE_COUNT_OP = 1

fpe = 0



"""
Load Fp arithmetic
"""
attach("fp12.sage")


"""
Load the parameters
"""
attach("params.sage")


"""
Load G2 arithmetic
"""
attach("ecn2.sage")


print "################################################################"
print "# Test G2 arithmetic"
print "################################################################"


print "\nG2_rand & hash"
for i in [1..NTEST2]:
	P = G2_rand()
	R = G2_mulDA(q, P)
	if R != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nG2_rand & hash Det"
for i in [1..NTEST2]:
	P = G2_randDet()
	R = G2_mulDA(q, P)
	if R != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\np_power_Frobenius"
for i in [1..NTEST2]:
	P = G2_rand()
	Q = G2_norm(p_power_FrobeniusJ(G2_toJ(P)))
	if Fp2_addC(Fp2_exp(Q[0], 3), B_invChi) != Fp2_exp(Q[1], 2):
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest GLV+wNAF"
for i in [1..NTEST2]:
	n  = rand()
	Q  = G2_rand()
	R1 = G2_norm(G2_mulGSwNAF(n, G2_toJ(Q), WB, BB))
	R2 = G2_norm(G2_mulDA(n, G2_toJ(Q)))

	diff = G2_addJJJ(R1, G2_negJ(R2))
	if diff != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest Simultaneous normalisation"
n = 256
PJ = [G2_mulDA(rand(),G2_toJ(G2_rand())) for i in range(0, n)]
PN = MultiNormalizeFp2(PJ, n)
for i in range(0, n):
	diff = G2_addJJJ(G2_norm(PJ[i]), G2_negJ(G2_toJ(PN[i])))
	if diff != G2_zeroJ():
		fpe +=1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0



print "\nTest Signed Comb mul"
for i in [1..NTEST2]:
	P = G2_rand()
	n = rand()
	PP = G2_precomputeSC(P)
	R1 = G2_norm(G2_mulDA(n, G2_toJ(P)))
	R2 = G2_norm(G2_mulSC(n, PP, PT, t256, Half_q))
	diff = G2_addJJJ(R1, G2_negJ(R2))
	if diff != G2_zeroJ():
		fpe += 1
		#print diff

if fpe != 0:
	print fpe, "errors!"
else:
	print "OK"
fpe = 0

