attach("ecn2.sage")
attach("fp12.sage")

"""
e(.,.) arithmetic
"""



# Granger-Scott special squaring
def Fp12_sqru(a):
	z0 = Fp2_copy(a[0][0])
	z4 = Fp2_copy(a[0][1])
	z3 = Fp2_copy(a[0][2])
	z2 = Fp2_copy(a[1][0])
	z1 = Fp2_copy(a[1][1])
	z5 = Fp2_copy(a[1][2])

	[t0, t1] = Fp4_square([z0, z1])

	#For A
	z0 = Fp2_subC(t0, z0)
	z0 = Fp2_addC(z0, z0)
	z0 = Fp2_addC(z0, t0)

	z1 = Fp2_addC(t1, z1)
	z1 = Fp2_addC(z1, z1)
	z1 = Fp2_addC(z1, t1)

	[t0, t1] = Fp4_square([z2, z3])
	[t2, t3] = Fp4_square([z4, z5])

	#For C
	z4 = Fp2_subC(t0, z4)
	z4 = Fp2_addC(z4, z4)
	z4 = Fp2_addC(z4, t0)

	z5 = Fp2_addC(t1, z5)
	z5 = Fp2_addC(z5, z5)
	z5 = Fp2_addC(z5, t1)

	#For B
	t0 = chi_mul(t3)
	z2 = Fp2_addC(t0, z2)
	z2 = Fp2_addC(z2, z2)
	z2 = Fp2_addC(z2, t0)

	z3 = Fp2_subC(t2, z3)
	z3 = Fp2_addC(z3, z3)
	z3 = Fp2_addC(z3, t2)


	return [[z0, z4, z3], [z2, z1, z5]]



# Fp12_exp w/ GS-sqru +NAFw2
def Fp12_expu(f, e):
	if e == 0:
		return 1
	if e == 1:
		return f
	if e == 2:
		g = Fp12_sqru(f)
		return g
	f_conj = Fp12_conj(f)

	bin_e, lbin = NAFw2(e)
	g = f[:2]
	for i in range(lbin-2,-1,-1):
		g = Fp12_sqru(g)
		if bin_e[i] == 1:
			g = Fp12_mul(g, f)
		if bin_e[i] == -1:
			g = Fp12_mul(g, f_conj)

	return g



def pointDblLineEval_woP(R):
	l = Fp6_zero()
#1
	t0    = Fp2_square(R[2])
	t4    = Fp2_mul(R[0], R[1])
	t1    = Fp2_square(R[1])
#2
	t3    = Fp2_addC(t0, t0)
	t4    = Fp2_mul(t4, [Half, Fp_zero()])
	t5    = Fp2_addC(t0, t1)
#3
	t0    = Fp2_addC(t0, t3)
#4 requires B = 2, chi = [1,1]. t2 = Fp2_mul(t0, B_invChi)
	t2    = Fp2_zero()
	t2[0] = Fp_addC(t0[0], t0[1])
	t2[1] = Fp_subC(t0[1], t0[0])
#5
	t0    = Fp2_square(R[0])
	t3    = Fp2_addC(t2, t2)
#6
	t3    = Fp2_addC(t2, t3)
	l[2]  = Fp2_addC(t0, t0)
#7
	R[0]  = Fp2_subC(t1, t3)
	l[2]  = Fp2_addC(l[2], t0)
	t3    = Fp2_addC(t1, t3)
#8
	R[0]  = Fp2_mul(t4, R[0])
	t3    = Fp2_mul(t3, [Half, Fp_zero()])
#9
	T0    = Fp2_square(t3)
	T1    = Fp2_square(t2)
#10
	T2    = Fp2_addC(T1, T1)
	t3    = Fp2_addC(R[1], R[2])
#11
	T2    = Fp2_addC(T2, T1)
	t3    = Fp2_square(t3)
#12
	t3    = Fp2_subC(t3, t5)
#13
	T0    = Fp2_subC(T0, T2)
#14
	R[1]  = T0#Fp2_mod(T0, [p,p])#Fp2_mod(R[1], T0)
	R[2]  = Fp2_mul(t1, t3)
	t2    = Fp2_subC(t2, t1)
#15
	l[0] = chi_mul(t2)
#16
	#See Fp6_mul_24_Fp_01
#17
	#See Fp6_mul_24_Fp_01
	l[1] = Fp2_neg(t3)

	return l, R



def Fp6_mul_24_Fp_01(l, P):
	l[2] = Fp2_mul_Fp(l[2], P[0])
	l[1] = Fp2_mul_Fp(l[1], P[1])

	return l



def pointDblLineEval(R, P):
	l, R = pointDblLineEval_woP(R)
	l    = Fp6_mul_24_Fp_01(l, P)

	return l, R



def pointAddLineEval_woP(R, Q):
	l = Fp6_zero()
# 1
	t1 = Fp2_mul(R[2], Q[0])
	t2 = Fp2_mul(R[2], Q[1])
# 2
	t1 = Fp2_subC(R[0], t1)
	t2 = Fp2_subC(R[1], t2)
# 3
	t3 = Fp2_square(t1)
# 4
	R[0] = Fp2_mul(t3, R[0])
	t4   = Fp2_square(t2)
# 5
	t3 = Fp2_mul(t3, t1)
	t4 = Fp2_mul(t4, R[2])
# 6
	t4 = Fp2_addC(t4, t3)
# 7
	t4 = Fp2_subC(t4, R[0])
# 8
	t4 = Fp2_subC(t4, R[0])
# 9
	R[0] = Fp2_subC(R[0], t4)
# 10
	T1 = Fp2_mul(t2, R[0])
	T2 = Fp2_mul(t3, R[1])
# 11
	T2 = Fp2_subC(T1, T2)
# 12
	R[1] = T2 #mod done
	R[0] = Fp2_mul(t1, t4)
	R[2] = Fp2_mul(t3, R[2])
# 14
	l[2] = Fp2_neg(t2)
# 15
	T1 = Fp2_mul(t2, Q[0])
	T2 = Fp2_mul(t1, Q[1])
# 16
	T1 = Fp2_subC(T1, T2)
# 17
	t2 = T1 #mod done
### @note: Be careful, below fomulas are typo.
# 18
	l[0] = chi_mul(t2)
	l[1] = t1


	return l, R



def pointAddLineEval(R, Q, P):
	l, R = pointAddLineEval_woP(R, Q)
	l    = Fp6_mul_24_Fp_01(l, P)

	return l, R



def Fp12_mulFp2_024(z, x):
	v  = Fp12_zero()
	z0 = Fp2_copy(z[0][0])
	z1 = Fp2_copy(z[0][1])
	z2 = Fp2_copy(z[0][2])
	z3 = Fp2_copy(z[1][0])
	z4 = Fp2_copy(z[1][1])
	z5 = Fp2_copy(z[1][2])
	x0 = Fp2_copy(x[0])
	x2 = Fp2_copy(x[2])
	x4 = Fp2_copy(x[1])

	D0 = Fp2_mul(z0, x0)
	D2 = Fp2_mul(z2, x2)
	D4 = Fp2_mul(z4, x4)
	t2 = Fp2_addC(z0, z4)
	t1 = Fp2_addC(z0, z2)
	s0 = Fp2_addC(z1, z3)
	s0 = Fp2_addC(s0, z5)

	S1 = Fp2_mul(z1, x2)
	T3 = Fp2_addC(S1, D4)
	T4 = chi_mul(T3)
	T4 = Fp2_addC(T4, D0)
	z0 = Fp2_copy(T4)

	T3 = Fp2_mul(z5, x4)
	S1 = Fp2_addC(S1, T3)
	T3 = Fp2_addC(T3, D2)
	T4 = chi_mul(T3)
	T3 = Fp2_mul(z1, x0)
	S1 = Fp2_addC(S1, T3)
	T4 = Fp2_addC(T4, T3)
	z1 = Fp2_copy(T4)

	t0 = Fp2_addC(x0, x2)
	T3 = Fp2_mul(t1, t0)
	T3 = Fp2_subC(T3, D0)
	T3 = Fp2_subC(T3, D2)
	T4 = Fp2_mul(z3, x4)
	S1 = Fp2_addC(S1, T4)
	T3 = Fp2_addC(T3, T4)
	t0 = Fp2_addC(z2, z4)
	z2 = Fp2_copy(T3)

	t1 = Fp2_addC(x2, x4)
	T3 = Fp2_mul(t0, t1)
	T3 = Fp2_subC(T3, D2)
	T3 = Fp2_subC(T3, D4)
	T4 = chi_mul(T3)
	T3 = Fp2_mul(z3, x0)
	S1 = Fp2_addC(S1, T3)
	T4 = Fp2_addC(T4, T3)
	z3 = Fp2_copy(T4)

	T3 = Fp2_mul(z5, x2)
	S1 = Fp2_addC(S1, T3)
	T4 = chi_mul(T3)
	t0 = Fp2_addC(x0, x4)
	T3 = Fp2_mul(t2, t0)
	T3 = Fp2_subC(T3, D0)
	T3 = Fp2_subC(T3, D4)
	T4 = Fp2_addC(T4, T3)
	z4 = Fp2_copy(T4)

	t0 = Fp2_addC(x0, x2)
	t0 = Fp2_addC(t0, x4)
	T3 = Fp2_mul(s0, t0)
	T3 = Fp2_subC(T3, S1)
	z5 = Fp2_copy(T3)

	v = [[z0,z1,z2],[z3,z4,z5]]
	return v



def Fp12_mulFp2_024_Fp2_024(x, y):
	v  = Fp12_zero()
	x0 = Fp2_copy(x[0])
	x2 = Fp2_copy(x[2])
	x4 = Fp2_copy(x[1])
	y0 = Fp2_copy(y[0])
	y2 = Fp2_copy(y[2])
	y4 = Fp2_copy(y[1])

	T00 = Fp2_mul(x0, y0)
	T22 = Fp2_mul(x2, y2)
	T44 = Fp2_mul(x4, y4)
	z0  = Fp2_addC(x0, x2)
	z1  = Fp2_addC(y0, y2)

	T02 = Fp2_mul(z0, z1)
	T02 = Fp2_subC(T02, T00)
	T02 = Fp2_subC(T02, T22)
	z2  = Fp2_copy(T02)

	z0  = Fp2_addC(x2, x4)
	z1  = Fp2_addC(y2, y4)
	T24 = Fp2_mul(z0, z1)
	T24 = Fp2_subC(T24, T22)
	T24 = Fp2_subC(T24, T44)
	T02 = chi_mul(T24)
	z3  = Fp2_copy(T02)

	z0  = Fp2_addC(x4, x0)
	z1  = Fp2_addC(y4, y0)
	T40 = Fp2_mul(z0, z1)
	T40 = Fp2_subC(T40, T00)
	T40 = Fp2_subC(T40, T44)
	z4  = Fp2_copy(T40)

	T02 = chi_mul(T22)
	z1  = Fp2_copy(T02)

	T02 = chi_mul(T44)
	T02 = Fp2_addC(T02, T00)
	z0  = Fp2_copy(T02)

	z5  = Fp2_zero()

	v = [[z0,z1,z2],[z3,z4,z5]]
	return v



def EasyExpo(f):
	t0 = f[:2]
	f  = Fp12_conj(f)
	t0 = Fp12_inv(t0)
	f  = Fp12_mul(t0, f)

	t0 = f[:2]
	for i in range(0,2):
		f = Fp12_powq(f, X)
	f = Fp12_mul(f, t0)

	return f



def Frobenius(f, n):
	e = f[:2]
	for i in range(0,n):
		e = Fp12_powq(e, X)

	return e



#Scott et al.
def HardExpo_Scottetal(g):
	f = g[:2]
	fx1 = Fp12_expu(f, abs(z))
	if z < 0:
		fx1 = Fp12_conj(fx1)
	fx2 = Fp12_expu(fx1, abs(z))
	if z < 0:
		fx2 = Fp12_conj(fx2)
	fx3 = Fp12_expu(fx2, abs(z))
	if z < 0:
		fx3 = Fp12_conj(fx3)

	xA = Fp12_mul(Fp12_inv(fx3), Frobenius(Fp12_inv(fx3),1))
	t0 = Fp12_sqru(xA)
	xB = Fp12_mul(Fp12_inv(fx1), Frobenius(Fp12_inv(fx2), 1))
	t0 = Fp12_mul(t0, xB)
	xB = Fp12_inv(fx2)
	t1 = Fp12_mul(t0, xB)
	xA = Frobenius(Fp12_inv(fx1), 1)
	xB = Fp12_inv(fx2)
	t0 = Fp12_mul(xA, xB)
	t0 = Fp12_mul(t0, t1)
	xB = Frobenius(fx2, 2)
	t1 = Fp12_mul(t1, xB)
	t0 = Fp12_sqru(t0)
	t0 = Fp12_mul(t0, t1)
	t1 = Fp12_sqru(t0)
	xB = Fp12_inv(f)
	t0 = Fp12_mul(t1, xB)
	xB = Fp12_mul(Fp12_mul(Frobenius(f,1), Frobenius(f,2)), Frobenius(f,3))
	t1 = Fp12_mul(t1, xB)
	t0 = Fp12_sqru(t0)
	t0 = Fp12_mul(t0, t1)

	return t0



# Karabina's compressed squaring
# Input: g \in \G_{\varpsi(6)}, g = [0, 0, g2, g3, g4, g5]
# Output: g^2 = [0, 0, g2, g3, g4, g5]^2
def Fp12_sqrKc(g):
	T0 = Fp2_square(g[4])
	T1 = Fp2_square(g[5])
#7
	T2 = chi_mul(T1)
#8
	T2 = Fp2_addC(T2, T0)
#9
	t2 = Fp2_copy(T2)
#1
	t0 = Fp2_addC(g[4], g[5])
	T2 = Fp2_square(t0)
#2
	T0 = Fp2_addC(T0, T1)
	T2 = Fp2_subC(T2, T0)
#3
	t0 = Fp2_copy(T2)
	t1 = Fp2_addC(g[2], g[3])
	T3 = Fp2_square(t1)
	T2 = Fp2_square(g[2])
#4
	t1 = chi_mul(t0)
#5
	g[2] = Fp2_addC(g[2], t1)
	g[2] = Fp2_addC(g[2], g[2])
#6
	g[2] = Fp2_addC(g[2], t1)
	t1 = Fp2_subC(t2, g[3])
	t1 = Fp2_addC(t1, t1)
#11
	T1 = Fp2_square(g[3])
#10
	g[3] = Fp2_addC(t1, t2)
#12
	T0 = chi_mul(T1)
#13
	T0 = Fp2_addC(T0, T2)
#14
	t0 = Fp2_copy(T0)
	g[4] = Fp2_subC(t0, g[4])
	g[4] = Fp2_addC(g[4], g[4])
#15
	g[4] = Fp2_addC(g[4], t0)
#16
	T2 = Fp2_addC(T2, T1)
	T3 = Fp2_subC(T3, T2)
#17
	t0 = Fp2_copy(T3)
	g[5] = Fp2_addC(g[5], t0)
	g[5] = Fp2_addC(g[5], g[5])
#18
	g[5] = Fp2_addC(g[5], t0)

	f = [Fp2_zero(), Fp2_zero(), g[2], g[3], g[4], g[5]]

	return f



# Karabina's compressed squaring reordering
# Input: [a0 + a1.w] + [b0 + b1.w]v + [c0 + c1.w]v^2
# Output: [g0, g1, g2, g3, g4, g5]
def fptogs(fp):
	gs    = [Fp2_zero() for i in range(0,6)]
	gs[0] = Fp2_copy(fp[0][0])
	gs[1] = Fp2_copy(fp[1][1])
	gs[2] = Fp2_copy(fp[1][0])
	gs[3] = Fp2_copy(fp[0][2])
	gs[4] = Fp2_copy(fp[0][1])
	gs[5] = Fp2_copy(fp[1][2])

	return gs



# Karabina's compressed squaring format change
# Input: [g0, g1, g2, g3, g4, g5]
# Output: [g0 + g1.w] + [g2 + g3.w]v + [g4 + g5.w]v^2
def gstofp(gs):
	f = Fp12_zero()
	f[0][0] = Fp2_copy(gs[0])
	f[0][1] = Fp2_copy(gs[1])
	f[0][2] = Fp2_copy(gs[2])
	f[1][0] = Fp2_copy(gs[3])
	f[1][1] = Fp2_copy(gs[4])
	f[1][2] = Fp2_copy(gs[5])

	return f



# Karabina's compressed squaring reordering function
# Input: [g0, g1, g2, g3, g4, g5]
# Output: [a0 + a1.w] + [b0 + b1.w]v + [c0 + c1.w]v^2
def reorderfpK(fp):
	f = Fp12_zero()
	f[0][0] = Fp2_copy(fp[0][0])
	f[0][1] = Fp2_copy(fp[1][1])
	f[0][2] = Fp2_copy(fp[1][0])
	f[1][0] = Fp2_copy(fp[0][2])
	f[1][1] = Fp2_copy(fp[0][1])
	f[1][2] = Fp2_copy(fp[1][2])

	return f



# Karabina's Sqr recover g_0 from g1,g2,g3,g4,g5
# Input:  [0, g1, g2, g3, g4, g5]
# Output: [g0, g1, g2, g3, g4, g5]
def Fp12_sqrKrecg0(fp):
	t0 = Fp2_square(fp[0][1])
	t1 = Fp2_mul(fp[1][0], fp[1][1])
	t0 = Fp2_subC(t0, t1)
	t0 = Fp2_addC(t0, t0)
	t0 = Fp2_subC(t0, t1)
	t1 = Fp2_mul(fp[0][2], fp[1][2])
	t0 = Fp2_addC(t0, t1)
	g  = chi_mul(t0)
	g  = Fp2_addC(g, Fp2_one())

	return g



# Karabina's Sqr recover g_1 from g2,g3,g4,g5
# Input:  [0, 0, g2, g3, g4, g5]
# Output: [g1_num, g1_den]
def Fp12_sqrKrecg1(fp):
	t = [Fp2_zero(), Fp2_zero()]
	if fp[0][2] != Fp2_zero():
		C12  = chi_mul(Fp2_square(fp[1][2]))
		C02  = Fp2_square(fp[1][1])
		t[0] = Fp2_copy(C02)
		C02  = Fp2_addC(C02, C02)
		C02  = Fp2_addC(C02, t[0])
		t[0] = Fp2_addC(C12, C02)
		t[0] = Fp2_subC(t[0], fp[1][0])
		t[0] = Fp2_subC(t[0], fp[1][0])
		t[1] = Fp2_addC(fp[0][2], fp[0][2])
		t[1] = Fp2_addC(t[1], t[1])
	else:
		t[0] = Fp2_mul(fp[1][1], fp[1][2])
		t[0] = Fp2_addC(t[0], t[0])
		t[1] = Fp2_copy(fp[1][0])

	return t



# Karabina's Sqr inversion of g1 denominator
#Input:  t  = [[g1_num, g1_den]_1, [g1_num, g1_den]_2, ...]
#Output: t0 = [[g1]_1, [g_1]_2, ...]
def Fp12_sqrKinvg1(t):
	n  = len(t)
	t0 = [Fp2_zero() for i in range(0, n)]

	f  = t[0][1]
	for i in range(1, n):
		f = Fp2_mul(f, t[i][1])
	f = Fp2_inv(f)

	if n==1:
		t0[0] = f
		return t0

	if n==2:
		t0[0] = Fp2_mul(t[0][0], Fp2_mul(f, t[1][1]))
		t0[1] = Fp2_mul(t[1][0], Fp2_mul(f, t[0][1]))
		return t0

	#for n>=3 use Simultaneous Montgomery Inversion
	d = [Fp2_one() for i in range(0, 2*n)]
	d[1]   = t[0][1]
	d[n+1] = t[n-1][1]
	for i in range(2,n):
		d[i]   = Fp2_mul(d[i-1], t[i-1][1])
		d[n+i] = Fp2_mul(d[n+i-1], t[n-i][1])

	for i in range(1, n-1):
		d[i] = Fp2_mul(d[i], d[2*n-i-1])

	d[0] = d[2*n-1]
	for i in range(0,n):
		d[i]  = Fp2_mul(d[i], f)
		t0[i] = Fp2_mul(t[i][0], d[i])

	return t0



def Fp12_sqrKd(gs, poles):
	n = len(gs)
	t = [[Fp2_one() for i in range(0,2)] for j in range(0, n)]

	fp = [gstofp(gs[i]) for i in range(0, n)]
	#recover g1
	for i in range(0, n):
		t[i] = Fp12_sqrKrecg1(fp[i])
	t = Fp12_sqrKinvg1(t)

	#recover g0
	for i in range(0,n):
		fp[i][0][1] = Fp2_copy(t[i])
		t[i] = Fp12_sqrKrecg0(fp[i])
		fp[i][0][0] = Fp2_copy(t[i])
		fp[i] = reorderfpK(fp[i])

	#multiply
	if poles[0] > 0:
		f = Fp12_copy(fp[0])
	else:
		f = Fp12_conj(fp[0])
	for i in range(1, n):
		if poles[i] > 0:
			f = Fp12_mul(f, Fp12_copy(fp[i]))
		else:
			f = Fp12_mul(f, Fp12_conj(fp[i]))

	return f



# Karabina exponentiation
#Input: f \in \G_{\varpsi_6}, z \in N w/very low-Hammming weight,
# or the parameter of the curve
#Output: f^z
# g0 = EasyExpo(rand12())
# Fp12_conj(Fp12_exp(g0,abs(z))) == Fp12_expK(g0)
def Fp12_expK(f, z0=None):
	global z
	if z0!=None:
		z = z0

	#get exponent (binary) poles
	zbits  = z.bits()
	znbits = z.nbits()
	poles = []
	for i in range(0, znbits):
		if zbits[i] != 0:
			poles.append(i*zbits[i])

	#special exponent case: 2^0
	if poles[0] == 0:
		poles.remove(0)

	n = len(poles)
	g  = [Fp2_zero() for i in range(0,6)]
	g  = fptogs(f)

	#Karabina's compressed squaring
	gs = []
	a  = [Fp2_copy(g[i]) for i in range(0,6)]
	pi = 0
	for i in range(0, n):
		for j in range(0, abs(poles[i])-pi):
			a = Fp12_sqrKc(a)
		gs.append([Fp2_copy(a[j]) for j in range(0,6)])
		pi = abs(poles[i])
	g = Fp12_sqrKd(gs, poles)

	#special exponent case: 2^0
	if zbits[0] > 0:
		g = Fp12_mul(g, Fp12_copy(f))
	elif zbits[0] < 0:
		g = Fp12_mul(g, Fp12_conj(f))

	return g



#Fuentes et al.
def HardExpo_Fuentesetal(g):
	f  = g[:2]
	fx = Fp12_expu(f, abs(z))
	if z < 0:
		fx = Fp12_conj(fx)
	f2x = Fp12_sqru(fx)
	f4x = Fp12_sqru(f2x)
	f6x = Fp12_mul(f4x,f2x)
	f6x2 = Fp12_expu(f6x, abs(z))
	if z < 0:
		f6x2 = Fp12_conj(f6x2)
	f6x3 = Fp12_expu(f6x2, abs(z))
	if z < 0:
		f6x3 = Fp12_conj(f6x3)
	f12x3 = Fp12_sqru(f6x3)


	a = Fp12_mul(f12x3, f6x2)
	a = Fp12_mul(a, f6x)
	b = Fp12_mul(a, Fp12_conj(f2x))

	f2 = Fp12_mul(a, f6x2)
	f2 = Fp12_mul(f2, f)
	f2 = Fp12_mul(f2, Frobenius(b,1))
	f2 = Fp12_mul(f2, Frobenius(a,2))

	f3 = Fp12_mul(b, Fp12_conj(f))
	f  = Fp12_mul(f2, Frobenius(f3,3))

	return f



def final_expGen(f):
	f = EasyExpo(f)
	#f = HardExpo_Scottetal(f)
	f = HardExpo_Fuentesetal(f)

	return f



# P, Q Normalized, P in AA, Q in JJ
def oap(P0, Q0):
	P  = G1_copyA(P0)
	if Q0[2] != Fp2_one():
		Q0 = G2_normJ(Q0)
	T  = G2_copyJ(Q0)
	Qn = G2_negJ(Q0)
	if z < 0:
		sign = -1
	lensiTbl = len(siTbl)

	f = Fp12_one()
	for i in range(lensiTbl-2,-1,-1):#range(2,lensiTbl):
		l, T = pointDblLineEval(T, P)
		f    = Fp12_square(f)
		f    = Fp12_mulFp2_024(f, l)

		if   siTbl[i] == 1:
			l, T = pointAddLineEval(T, Q0, P)
			f    = Fp12_mulFp2_024(f, l)
		elif siTbl[i] == -1:
			l, T = pointAddLineEval(T, Qn, P)
			f    = Fp12_mulFp2_024(f, l)

	Q1 = p_power_FrobeniusJ(Q0)
	Q2 = p_power_FrobeniusJ(Q1)

	if sign == -1:
		f = Fp12_conj(f)
		T = G2_negJ(T)

	Q2 = G2_negJ(Q2)
	l, T  = pointAddLineEval(T, Q1, P)
	l1, T = pointAddLineEval(T, Q2, P)
	ft    = Fp12_mulFp2_024_Fp2_024(l, l1)
	f     = Fp12_mul(f, ft)

	f = final_expGen(f) #we will end up using the one from the F.E. Chapter


	return f
